package gui;

import javax.swing.JFrame;

public class ScreenFrame extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	ScreenPanel screenPanel;
	static ScreenFrame screenFrame;

	public ScreenFrame() {
		screenPanel = ScreenPanel.getScreenPanel();
		this.add(screenPanel);
		this.setSize(800, 600);
		this.setVisible(true);
		this.setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
		screenFrame = this;
	}

	public static ScreenFrame getScreenFrame() {
		if (screenFrame == null)
			screenFrame = new ScreenFrame();
		return screenFrame;
	}
}
