package gui;

import java.awt.FlowLayout;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

@SuppressWarnings("serial")
public class MainFrame extends JFrame {
	JTextField txfIP, txfPort;
	JLabel lblIP, lblPort;
	JButton btnConnect, btnTerminate;
	static MainFrame mainFrame;

	public MainFrame() {
		super();
		txfIP = new JTextField(10);
		txfPort = new JTextField(5);
		lblIP = new JLabel("IP");
		lblPort = new JLabel("Port");
		btnConnect = new JButton("Connect");
		btnTerminate = new JButton("Terminate");
		this.setLayout(new FlowLayout());
		this.add(lblIP);
		this.add(txfIP);
		this.add(lblPort);
		this.add(txfPort);
		this.add(btnConnect);
		this.add(btnTerminate);
		this.setSize(500, 200);
		this.setVisible(true);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		mainFrame = this;
	}

	public void addActionListener(ActionListener actionListener) {
		btnConnect.addActionListener(actionListener);
		btnTerminate.addActionListener(actionListener);
	}

	public static MainFrame getMainFrame() {
		if (mainFrame == null)
			mainFrame = new MainFrame();
		return mainFrame;
	}

	public String getIP() {
		return txfIP.getText();
	}

	public String getPort() {
		return txfPort.getText();
	}
}