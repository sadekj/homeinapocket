package gui;

import java.awt.Color;
import java.awt.Container;
import java.awt.GridLayout;
import java.awt.Label;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import network.ConnectionHandler;


@SuppressWarnings("serial")
public class LogInFrame extends JFrame
{
	private JPanel pan1; // ip + port panel
	private JPanel pan2; // password panel
	
	private JLabel lblServerIp;
	private JLabel lblPort;
	
	private JTextField txtServerIp;
	private JTextField txtPort;
	
	private JButton btnConnect;

	
	public LogInFrame() 
	{
		super();
		
		
		pan1 = new JPanel();
        pan2 = new JPanel();

        lblPort = new JLabel("Port");
        lblServerIp = new JLabel("Server IP");

        txtPort = new JTextField();
        txtServerIp = new JTextField();
        
        btnConnect = new JButton("Connect");


        
        //// panel1 configuration
        
        pan1.setBorder(BorderFactory.createLineBorder(new Color(0, 0, 0)));

        pan1.setLayout(new GridLayout( 3, 5, 5, 5));
        
        pan1.add(new Label());
        pan1.add(lblServerIp);
        lblServerIp.setHorizontalAlignment(SwingConstants.CENTER);
        pan1.add(new Label());
        pan1.add(lblPort);
        lblPort.setHorizontalAlignment(SwingConstants.CENTER);

        pan1.add(new Label());

        pan1.add(new Label());
        pan1.add(txtServerIp);
        pan1.add(new Label());
        pan1.add(txtPort);
        pan1.add(new Label());
        
        pan1.add(new Label());
        pan1.add(new Label());
        pan1.add(btnConnect);
        pan1.add(new Label());
        pan1.add(new Label());
        
        //////// panel2 configurtion
          
        pan2.setBorder(BorderFactory.createLineBorder(new Color(0, 0, 0)));
        
        
        ////////   jframe configuration
        
        Container contentPane = this.getContentPane();
        contentPane.setLayout(new GridLayout(2, 1, 20 , 20));
        
        contentPane.add(pan1);
        contentPane.add(pan2);
        
        setName("Log In");
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setLocationRelativeTo(null);
        pack();
        
        ////////////////////////////////////activate btns
        
        btnConnect.addActionListener(new ActionListener()  /// log in button
        {
        	public void actionPerformed( ActionEvent evt ) 
        	{           
        		String ip = txtServerIp.getText();
        		String port = txtPort.getText();
        		
        		if( !ip.equals("") && !port.equals("") )
        		{
        			ConnectionHandler.getConnectionHandler().connect(ip, port);
        			ConnectionHandler.getConnectionHandler().sendCommand("password=READY");
        		}
        		else
        			JOptionPane.showMessageDialog(null, "Please insert both ip and port numbers");
        			
        		dispose();
        	}
        } );
        
	}
	
	

}
