package gui;

import engine.ButtonsActionListener;
import global.Global;

import java.awt.Graphics;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import network.ConnectionHandler;

@SuppressWarnings("serial")
public class ScreenPanel extends JPanel implements MouseMotionListener,
		MouseListener, KeyListener {
	Graphics g;
	BufferedImage[] blocks;
	BufferedImage screen;
	int blockWidth, blockHeight, columns = 4, rows = 4, marginLeft, marginTop;
	private BufferedImage image;
	static ScreenPanel screenPanel;

	float deviceX, deviceY, respectiveX, respectiveY, oldX, oldY;

	public ScreenPanel() {
		super();
		// try {
		// image = ImageIO.read(new File("screen.gif"));
		// } catch (IOException e) {
		// // TODO Auto-generated catch block
		// e.printStackTrace();
		// }
		blocks = new BufferedImage[16];
		this.setSize(800, 600);
		this.addMouseListener(this);
		this.addMouseMotionListener(this);
		this.addKeyListener(this);
		screenPanel = this;
	}

	public static ScreenPanel getScreenPanel() {
		if (screenPanel == null)
			screenPanel = new ScreenPanel();
		return screenPanel;
	}

	private void render(Graphics g) {
		Global.log("RENDERING!");
		if (blocks != null)
			for (int i = 0; i < blocks.length; i++) {
				int left = (i % columns) * (this.getWidth() / columns);
				left += this.marginLeft;
				int top = (i / rows) * (this.getHeight() / rows);
				top += this.marginTop;
				g.drawImage(blocks[i], top, left, null);
			}
		else if (screen != null) {
			g.drawImage(screen, 0, 0, null);
			screen = null;
		} else {
		}
	}

	@Override
	public void paint(Graphics g) {
		super.paintComponent(g);
		try {
			if (ConnectionHandler.loadFirst) {
				image = ImageIO.read(new File("screen.gif"));
				g.drawImage(image, 0, 0, null);
				ConnectionHandler.loadFirst = false;
			} else {
				for (int i = 0; i < blocks.length; i++) {
					int x = i + 1;
					blocks[i] = ImageIO.read(new File("screen" + x + ".gif"));
					int left = (i % columns) * (this.getWidth() / columns);
					left += this.marginLeft;
					int top = (i / rows) * (this.getHeight() / rows);
					top += this.marginTop;
					double newX = this.getWidth() / (double) rows;
					double scaleX = newX / blocks[i].getWidth();
					double newY = this.getHeight() / (double) columns;
					double scaleY = newY / blocks[i].getHeight();
					image = resizeBufferedImage(blocks[i], scaleX, scaleY);
					g.drawImage(image, left, top, null);
					ButtonsActionListener.ScreenUpdating.changes[i] = false;
				}
			}
		} catch (IOException e) {
			Global.logError("Failed loading screen");
		}
		// render(g);
		ScreenFrame.getScreenFrame().repaint();
	}

	public void loadFilesToBufferedImage(File[] files) throws IOException {
		for (int i = 0; i < files.length; i++) {
			if (files[i] != null)
				loadFileToBufferedImage(i, files[i]);
		}
	}

	public void loadFileToBufferedImage(int index, File file)
			throws IOException {
		BufferedImage in = ImageIO.read(file);
		blocks[index] = new BufferedImage(in.getWidth(), in.getHeight(),
				BufferedImage.TYPE_INT_ARGB);
		repaint();
	}

	public void loadFileToBufferedImage(File file) throws IOException {
		BufferedImage in = ImageIO.read(file);
		screen = new BufferedImage(in.getWidth(), in.getHeight(),
				BufferedImage.TYPE_INT_ARGB);
		repaint();
	}

	public BufferedImage resizeBufferedImage(BufferedImage before,
			double widthRatio, double hegihtRatio) {
		int w = before.getWidth();
		int h = before.getHeight();
		BufferedImage after = new BufferedImage(w, h,
				BufferedImage.TYPE_INT_ARGB);
		AffineTransform at = new AffineTransform();
		at.scale(widthRatio, hegihtRatio);
		AffineTransformOp scaleOp = new AffineTransformOp(at,
				AffineTransformOp.TYPE_BILINEAR);
		after = scaleOp.filter(before, after);
		return after;
	}

	private void prepareCalculation(MouseEvent event) {
		this.respectiveX += event.getX() - oldX;
		this.respectiveY += event.getY() - oldY;
		this.deviceX = event.getX();
		this.deviceY = event.getY();
		this.respectiveX -= marginLeft;
		this.respectiveY -= marginTop;
		this.respectiveX = ((this.deviceX) / this.getWidth())
				* (float) ConnectionHandler.blockWidth * 4;
		this.respectiveY = ((this.deviceY) / this.getHeight())
				* (float) ConnectionHandler.blockHeight * 4;
		oldX = this.respectiveX;
		oldY = this.respectiveY;
	}

	@Override
	public void mouseDragged(MouseEvent e) {

	}

	@Override
	public void mouseMoved(MouseEvent e) {
		prepareCalculation(e);
		ConnectionHandler.getConnectionHandler().sendCommand(
				"1:" + (int) this.respectiveX + "," + (int) this.respectiveY);
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		prepareCalculation(e);
		if (SwingUtilities.isLeftMouseButton(e))
			ConnectionHandler.getConnectionHandler().sendCommand(
					"2:" + (int) this.respectiveX + ","
							+ (int) this.respectiveY);
		else if (SwingUtilities.isRightMouseButton(e))
			ConnectionHandler.getConnectionHandler().sendCommand(
					"4:" + (int) this.respectiveX + ","
							+ (int) this.respectiveY);
	}

	@Override
	public void mousePressed(MouseEvent e) {
	}

	@Override
	public void mouseReleased(MouseEvent e) {

	}

	@Override
	public void mouseEntered(MouseEvent e) {

	}

	@Override
	public void mouseExited(MouseEvent e) {

	}

	@Override
	public void keyTyped(KeyEvent e) {

	}

	@Override
	public void keyPressed(KeyEvent e) {
		ConnectionHandler.getConnectionHandler().sendCommand(
				"6:p=" + e.getKeyCode());
	}

	@Override
	public void keyReleased(KeyEvent e) {
		ConnectionHandler.getConnectionHandler().sendCommand(
				"6:r=" + e.getKeyCode());
	}
}
