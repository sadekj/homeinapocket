package network;

import global.Global;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.Socket;

/**
 * This class provides all the needed network functionalities
 * 
 * @author Sadek Jabr
 * 
 */

public class ConnectionHandler {
	public static final int KEYBOARD = 1, MOUSE = 2, SYSTEM = 3, TERMINATE = 4,
			SCREEN = 5, POLITE = 6, PASSWORD = 7;
	int action;
	static boolean connected;
	DataOutputStream outToServer;
	BufferedReader inFromServer;
	Socket clientSocket;
	InputStream inputStream;
	static ConnectionHandler connectionHandler;
	static boolean[] changes;
	static boolean changed = false;
	public static int blockWidth, blockHeight;
	static int recievedNum = 0, sentNum = 0;
	public static boolean loadFirst = true;
	public static int MODE;

	/**
	 * This function will create a connection with the inputed ip and port
	 * 
	 * @param IP
	 *            , the IP address to connect to.
	 * @param port
	 *            , the port to connect through
	 */
	public boolean connect(String IP, String port) {
		loadFirst = true;
		try {
			Global.log("Creating socket connection to" + IP + ":" + port);
			clientSocket = new Socket(IP, Integer.parseInt(port));
			outToServer = new DataOutputStream(clientSocket.getOutputStream());
			inFromServer = new BufferedReader(new InputStreamReader(
					clientSocket.getInputStream()));
			inputStream = clientSocket.getInputStream();
			connected = true;
			Global.log("Connected");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return connected;
	}

	/**
	 * This function will terminate an active connection.
	 * 
	 * @return boolean, true if disconnected, false otherwise
	 * @author Sadek Jabr
	 */
	public boolean terminate() {
		if (clientSocket.isConnected()) {
			try {
				sendCommand("terminate=true");
				connected = false;
				clientSocket.close();
			} catch (IOException e) {
				Global.log("Failed termination");
			}
		}
		return !connected;
	}

	/**
	 * This is just to satisfy the single object creation, and will create one
	 * if there is no object from it
	 * 
	 * @return ConnectionHandler, which is the object that is the connection
	 *         created on
	 */
	public static ConnectionHandler getConnectionHandler() {
		if (connectionHandler == null)
			connectionHandler = new ConnectionHandler();
		return connectionHandler;
	}

	/**
	 * This function will send a giving message as a new line
	 * 
	 * @param strCommand
	 *            , the message to send
	 * @return boolean, true if success sent, false otherwise
	 * @author Sadek Jabr
	 */
	public boolean sendCommand(String strCommand) {
		if (connected && outToServer != null) {
			try {
				sentNum++;
				Global.log(sentNum + " " + "Sending command:" + strCommand);
				outToServer.writeBytes(strCommand + "\n");
				return true;
			} catch (IOException e) {
				Global.log("Faild sending command " + strCommand);
			}
		}
		return false;
	}

	/**
	 * This function returns one line message from the stream or null if
	 * something is wrong, and will wait for a message if no message is there
	 * 
	 * @return String, the message from the stream
	 * @author Sadek Jabr
	 */
	public String receiveCommand() {
		try {
			if (connected && inFromServer != null && inFromServer.ready()) {
				String command = inFromServer.readLine();
				recievedNum++;
				Global.log(recievedNum + "Command Received:" + command);
				if (command.equals("terminate=true"))
					terminate();
				return command;
			}
		} catch (IOException e) {
			Global.log("Faild Receiving data");
		}
		return null;
	}

	public byte[] getScreen() {
		try {
			if (connected && inFromServer != null && inFromServer.ready()) {
				byte[] buffer = new byte[Integer.parseInt(inFromServer
						.readLine())];
				// String str = inFromServer.readLine();
				clientSocket.getInputStream().read(buffer);
				// buffer = str.getBytes();
				Global.log("buffer= " + buffer);
				return buffer;
			}
		} catch (IOException e) {
			Global.log("Faild Receiving data");
		}
		return null;
	}

	/**
	 * Receive an image file from a connected socket and save it to a file.
	 * <p>
	 * the first 4 bytes it receives indicates the file's size
	 * </p>
	 * 
	 * @param is
	 *            InputStream from the connected socket
	 * @param fileName
	 *            Name of the file to save in external storage, without
	 *            File.separator
	 * @return Bitmap representing the image received o null in case of an error
	 * @throws Exception
	 * @see {@link sendFile} for an example how to send the file at other side.
	 * 
	 */
	public File receiveFile(String fileName) throws Exception {

		String fileInES = fileName;

		// read 4 bytes containing the file size
		byte[] bSize = new byte[4];
		int offset = 0;
		while (offset < bSize.length) {
			int bRead = inputStream.read(bSize, offset, bSize.length - offset);
			offset += bRead;
		}
		// Convert the 4 bytes to an int
		int fileSize;
		fileSize = (int) (bSize[0] & 0xff) << 24
				| (int) (bSize[1] & 0xff) << 16 | (int) (bSize[2] & 0xff) << 8
				| (int) (bSize[3] & 0xff);

		// buffer to read from the socket
		// 8k buffer is good enough
		byte[] data = new byte[8 * 1024];

		int bToRead;
		FileOutputStream fos = new FileOutputStream(fileInES);
		BufferedOutputStream bos = new BufferedOutputStream(fos);
		while (fileSize > 0) {
			// make sure not to read more bytes than filesize
			if (fileSize > data.length)
				bToRead = data.length;
			else
				bToRead = fileSize;
			int bytesRead = inputStream.read(data, 0, bToRead);
			if (bytesRead > 0) {
				bos.write(data, 0, bytesRead);
				fileSize -= bytesRead;
			}
		}
		bos.close();

		// Convert the received image to a Bitmap
		// If you do not want to return a bitmap comment/delete the folowing
		// lines
		// and make the function to return void or whatever you prefer.
		return new File(fileInES);
	}

	public File[] receiveScreen(boolean[] changes, File[] blocks) {
		try {
			Global.log("Expecting segment size and changed segments");
			String command = receiveCommand();
			if (!connected)
				return blocks;
			while (command == null || command.contains(":"))
				command = receiveCommand();
			sendCommand("READY1");
			if (command == null || command.equals("null"))
				return blocks;
			String[] tokens = command.split(",");
			blockWidth = Integer.parseInt(tokens[2]);
			blockHeight = Integer.parseInt(tokens[3]);
			int blockIndex;
			for (int i = 4; i < tokens.length; i++) {
				blockIndex = Integer.parseInt(tokens[i]);
				changes[blockIndex] = true;
				blocks[blockIndex++] = receiveFile("screen" + blockIndex + "."
						+ Global.getScreenFormat().toLowerCase());
			}
		} catch (Exception e) {
			Global.log("Failed reciving screenblocks");
			e.printStackTrace();
			connected=false;
		}
		return blocks;
	}

	/**
	 * This function will return if connected or not
	 * 
	 * @return boolean if there is connection or not.
	 */
	public boolean isConnected() {
		return connected;
	}

	/**
	 * This Internal class handles the connection as a separate thread than the
	 * main thread
	 * 
	 * @author Sadek Jabr
	 * 
	 */
	public class BackLoading implements Runnable {
		String ip;
		int port;

		@Override
		public void run() {

		}

		public void setIPPort(String ip, int port) {
			this.ip = ip;
			this.port = port;
		}
	}
}
