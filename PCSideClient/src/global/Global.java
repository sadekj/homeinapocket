package global;

import java.util.Date;
import java.util.HashMap;

/**
 * This class is for providing functions that we will use through out the
 * project
 * 
 * @author Abdallah.
 */
public class Global {
	public static HashMap<String, String> CONFIGURATION = new HashMap<String, String>();

	/**
	 * This will print the inputed object and next to it the time it is printed
	 * 
	 * @param obj
	 *            , the object to be printed
	 */
	public static void log(Object obj) {
		System.out.println("[" + new Date() + "] " + obj);
	}

	/**
	 * This will print the inputed object <b>as error</b> and next to it the
	 * time it is printed
	 * 
	 * @param obj
	 *            , the object to be printed
	 */
	public static void logError(Object obj) {
		System.err.println("[" + new Date() + "] " + obj);
	}

	public static int getRows() {
		int value = Integer.parseInt(CONFIGURATION.get("rows"));
		if (value == 0)
			value = 4;
		return value;
	}

	public static int getColumns() {
		int value = Integer.parseInt(CONFIGURATION.get("columns"));
		if (value == 0)
			value = 4;
		return value;
	}

	public static String getScreenFormat() {
		String format = CONFIGURATION.get("screenFormat");
		if (format != null)
			return format;
		else
			return "GIF";
	}
}
