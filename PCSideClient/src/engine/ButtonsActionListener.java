package engine;

import gui.MainFrame;
import gui.ScreenFrame;
import gui.ScreenPanel;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import network.ConnectionHandler;

public class ButtonsActionListener implements ActionListener {

	@Override
	public void actionPerformed(ActionEvent e) {
		switch (e.getActionCommand()) {
		case "Connect":
			ConnectionHandler.getConnectionHandler().connect(
					MainFrame.getMainFrame().getIP(),
					MainFrame.getMainFrame().getPort());
			ConnectionHandler.getConnectionHandler().sendCommand("POLITE");
			ConnectionHandler.getConnectionHandler().sendCommand("screen=true");
			new ScreenFrame();
			Thread t = new Thread(new ScreenUpdating());
			t.start();
			break;
		case "Terminate":
			ScreenFrame.getScreenFrame().setVisible(false);
			ConnectionHandler.getConnectionHandler().terminate();
		default:
			break;
		}
	}

	public static class ScreenUpdating implements Runnable {
		static File[] blocks = new File[16];
		public static boolean[] changes = new boolean[blocks.length];

		@Override
		public void run() {
			try {
				File screen = ConnectionHandler.getConnectionHandler()
						.receiveFile("screen.gif");
				ScreenPanel.getScreenPanel().loadFileToBufferedImage(screen);
				while (ConnectionHandler.getConnectionHandler().isConnected()) {
					ConnectionHandler.getConnectionHandler().receiveScreen(
							changes, blocks);
					ScreenPanel.getScreenPanel().loadFilesToBufferedImage(
							blocks);
					changes = new boolean[blocks.length];
				}
			} catch (Exception e) {
				e.printStackTrace();
				return;
			}
		}
	}
}
