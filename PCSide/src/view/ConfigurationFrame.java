package view;

import engine.Robot;
import global.Global;

import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.TextField;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

public class ConfigurationFrame extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	TextField txtRows, txtColumns, txtScreenFormat;
	JPanel pMain, pSegments, pScreenFormat;
	JButton btnSaveConfiguration;
	static ConfigurationFrame configurationFrame;

	public ConfigurationFrame() {
		this.setTitle("Configuration");
		this.setSize(250, 150);
		this.setLocationRelativeTo(null);
		this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		this.setVisible(true);

		pMain = new JPanel();
		pSegments = new JPanel();
		pScreenFormat = new JPanel();

		JLabel lSegments = new JLabel("Segments");
		txtRows = new TextField(5);
		txtColumns = new TextField(5);

		JLabel lScreenFormat = new JLabel("Screen Format");
		txtScreenFormat = new TextField(10);

		btnSaveConfiguration = new JButton("Save Configurations");

		pSegments.setLayout(new FlowLayout());
		pSegments.add(lSegments);
		pSegments.add(txtRows);
		pSegments.add(txtColumns);
		pSegments.setBackground((new Color(137, 233, 233)));

		pScreenFormat.setLayout(new FlowLayout());
		pScreenFormat.add(lScreenFormat);
		pScreenFormat.add(txtScreenFormat);
		pScreenFormat.setBackground((new Color(137, 233, 233)));

		pMain.setLayout(new GridLayout(3, 1));
		pMain.add(pSegments);
		pMain.add(pScreenFormat);
		pMain.add(btnSaveConfiguration);

		pMain.setBackground((new Color(137, 233, 233)));
		this.add(pMain);
		configurationFrame = this;
		updateValues();
	}

	public static ConfigurationFrame getConfigurationFrame() {
		if (configurationFrame == null)
			configurationFrame = new ConfigurationFrame();
		return configurationFrame;
	}

	public void saveValues() {
		try {
			Global.setRows(Integer.parseInt(txtRows.getText()));
			Global.setColumns(Integer.parseInt(txtColumns.getText()));
			Global.setScreenFormat(txtScreenFormat.getText());
			Robot.firstRun = true;
			Robot.blocksFiles = new File[Global.getRows() * Global.getColumns()];
			Robot.blocks = new BufferedImage[Global.getRows()
					* Global.getColumns()];
			Robot.changes = new boolean[Global.getRows() * Global.getColumns()];
			JOptionPane.showMessageDialog(this, "Saved!");
		} catch (Exception e) {
			JOptionPane.showMessageDialog(this,
					"Failed saving data, make sure you entered valid data");
		}
	}

	public void updateValues() {
		txtColumns.setText(Global.getColumns() + "");
		txtRows.setText(Global.getRows() + "");
		txtScreenFormat.setText(Global.getScreenFormat());
	}

	public void addActionListener(ActionListener actionListener) {
		btnSaveConfiguration.addActionListener(actionListener);
	}
}
