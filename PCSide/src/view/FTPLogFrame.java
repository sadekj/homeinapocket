package view;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionListener;
import java.util.Date;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.text.DefaultCaret;

public class FTPLogFrame extends JFrame {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	JLabel lblLocationToBeSavedin, lblFileLocation, lblFileName;
	JTextField txfLocationToBeSavedin, txfFileLocation, txfFileName;
	JButton btnSend, btnReceive;
	JPanel panelActions;
	JTextArea txtConsole;
	JScrollPane scrollPane;
	static FTPLogFrame ftpLogFrame;

	public FTPLogFrame() {
		txfLocationToBeSavedin = new JTextField(10);
		txfFileLocation = new JTextField(10);
		txfFileName = new JTextField(10);
		btnReceive = new JButton("Receive File");
		btnSend = new JButton("Send File");
		lblLocationToBeSavedin = new JLabel("Location To Be Saved In");
		lblFileLocation = new JLabel("File Location");
		lblFileName = new JLabel("File Name");
		panelActions = new JPanel(new FlowLayout());
		panelActions.add(lblLocationToBeSavedin);
		panelActions.add(txfLocationToBeSavedin);
		panelActions.add(lblFileLocation);
		panelActions.add(txfFileLocation);
		panelActions.add(lblFileName);
		panelActions.add(txfFileName);
		panelActions.add(btnSend);
		panelActions.add(btnReceive);
		this.setLayout(new BorderLayout());
		this.add(BorderLayout.NORTH, panelActions);

		txtConsole = new JTextArea();
		scrollPane = new JScrollPane(txtConsole);
		txtConsole.setEditable(false);
		DefaultCaret caret = (DefaultCaret) txtConsole.getCaret();
		caret.setUpdatePolicy(DefaultCaret.ALWAYS_UPDATE);
		this.add(BorderLayout.CENTER, scrollPane);
		this.setSize(800, 200);
		this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		this.setLocationRelativeTo(null);
		ftpLogFrame = this;
	}

	public static void log(Object obj) {
		if (ftpLogFrame != null) {
			ftpLogFrame.txtConsole.setText(ftpLogFrame.txtConsole.getText()
					+ "\n[" + new Date() + "]" + obj.toString());
		}
	}

	public static FTPLogFrame getftpLogFrame() {
		if (ftpLogFrame == null)
			ftpLogFrame = new FTPLogFrame();
		return ftpLogFrame;
	}

	public String getLog() {
		return txtConsole.getText();
	}

	public String getLocationToBeSavedin() {
		return txfLocationToBeSavedin.getText();
	}

	public String getFileLocation() {
		return txfFileLocation.getText();
	}

	public String getFileName() {
		return txfFileName.getText();
	}

	public String getPath() {
		return getFileLocation() + getFileName();
	}

	public boolean FileAlreadyExists() {
		int reply = JOptionPane.showConfirmDialog(null,
				"File Already Exists. Want to OverWrite?", "Home In a Pocket",
				JOptionPane.YES_NO_OPTION);
		if (reply == JOptionPane.YES_OPTION)
			return true;
		else
			return false;
	}

	public void addActionListener(ActionListener actionListener) {
		btnReceive.addActionListener(actionListener);
		btnSend.addActionListener(actionListener);
	}
}
