package view;

import global.Global;

import java.awt.Color;
import java.awt.FlowLayout;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class IPPortPanel extends JPanel {

	/**
	 * 
	 */
	JTextField tfIP, tfPort, tfExternalIP;
	JLabel lIP, lPort, lExternalIP;
	static IPPortPanel ipPortPanel;
	private static final long serialVersionUID = 1L;

	public IPPortPanel() {
		tfIP = new JTextField("Internal IP", 15);
		lIP = new JLabel("Internal IP: ", JLabel.RIGHT);
		tfExternalIP = new JTextField("External IP", 15);
		lExternalIP = new JLabel("External IP: ", JLabel.RIGHT);
		lPort = new JLabel("Port: ", JLabel.CENTER);
		tfPort = new JTextField("Port", 5);
		tfIP.setEditable(false);
		tfPort.setEditable(false);
		this.setLayout(new FlowLayout());
		this.add(lExternalIP);
		this.add(tfExternalIP);
		this.add(lIP);
		this.add(tfIP);
		this.add(lPort);
		this.add(tfPort);
		this.setBackground(new Color(137, 233, 233));
		ipPortPanel = this;
	}

	public static IPPortPanel getIpPortPanel() {
		if (ipPortPanel == null)
			ipPortPanel = new IPPortPanel();
		return ipPortPanel;
	}

	public void updateValues() {
		tfIP.setText(Global.getIP());
		tfPort.setText(Global.getPort() + "");
		tfExternalIP.setText(Global.getExternalIP());
	}

}
