package view;

import java.awt.Color;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.text.DefaultCaret;

public class LogFrame extends JFrame {
	private static final long serialVersionUID = 1L;
	JTextArea txtConsole;
	JScrollPane scrollPane;
	static LogFrame LogFrame;

	public LogFrame() {
		txtConsole = new JTextArea();
		scrollPane = new JScrollPane(txtConsole);
		txtConsole.setEditable(false);
		txtConsole.setBackground(new Color(137, 233, 233));
		DefaultCaret caret = (DefaultCaret) txtConsole.getCaret();
		caret.setUpdatePolicy(DefaultCaret.ALWAYS_UPDATE);
		this.add(scrollPane);
		this.setSize(500, 500);
		this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		this.setLocationRelativeTo(null);
		LogFrame = this;
	}

	public static void log(Object obj) {
		if (LogFrame != null) {
			LogFrame.txtConsole.setText(LogFrame.txtConsole.getText() + "\n"
					+ obj.toString());
		}
	}

	public static LogFrame getLogFrame() {
		if (LogFrame == null)
			LogFrame = new LogFrame();
		return LogFrame;
	}

	public String getLog() {
		return txtConsole.getText();
	}
}
