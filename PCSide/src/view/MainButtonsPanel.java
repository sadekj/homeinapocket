package view;

import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JPanel;

public class MainButtonsPanel extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JButton btnLog;
	private JButton btnCustomedCommands;
	private JButton btnSaveLogToFile;
	private JButton btnHelp;
	private JButton btnFTP;
	private JButton btnSetPassword;
	private JButton btnConfFile;
	public MainButtonsPanel() {
		btnLog = new JButton("LOG");
		btnSaveLogToFile = new JButton("Save LOG to File");
		btnCustomedCommands = new JButton("Customed Commands");
		btnHelp = new JButton("HELP");
		btnFTP = new JButton("FTP");
		btnSetPassword = new JButton("Set Password");
		btnConfFile = new JButton("Configuration File");
		this.setLayout(new GridLayout(4, 2, 10, 10));
		this.add(btnLog);
		this.add(btnSaveLogToFile);
		this.add(btnCustomedCommands);
		this.add(btnHelp);
		this.add(btnFTP);
		this.add(btnSetPassword);
		this.add(btnConfFile);
		this.setBackground(new Color(137, 233, 233));
	}
	public void addActionListener(ActionListener actionListener){
		btnLog.addActionListener(actionListener);
		btnCustomedCommands.addActionListener(actionListener);
		btnSaveLogToFile.addActionListener(actionListener);
		btnHelp.addActionListener(actionListener);
		btnFTP.addActionListener(actionListener);
		btnSetPassword.addActionListener(actionListener);
		btnConfFile.addActionListener(actionListener);
	}
}
