package view;

import java.awt.BorderLayout;
import java.awt.Color;

import javax.swing.JFrame;

import engine.ButtonsActionListener;

public class MainFrame extends JFrame {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	IPPortPanel ipPortPanel;
	MainButtonsPanel mainButtonsPanel;
	LogFrame logFrame;
	ConfigurationFrame configurationFrame;
	FTPLogFrame ftpLogFrame;
	ButtonsActionListener buttonsActionListener;

	public MainFrame() {
		ipPortPanel = new IPPortPanel();
		mainButtonsPanel = new MainButtonsPanel();
		logFrame = new LogFrame();
		buttonsActionListener = new ButtonsActionListener();
		configurationFrame = new ConfigurationFrame();
		ftpLogFrame = new FTPLogFrame();
		mainButtonsPanel.addActionListener(buttonsActionListener);
		configurationFrame.addActionListener(buttonsActionListener);
		ftpLogFrame.addActionListener(buttonsActionListener);
		this.setTitle("Server Side");
		this.setSize(700, 230);
		this.setLocationRelativeTo(null);
		this.setLayout(new BorderLayout());
		this.add(BorderLayout.NORTH, ipPortPanel);
		this.add(BorderLayout.CENTER, mainButtonsPanel);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.setBackground(new Color(137, 233, 233));
		this.setVisible(true);
	}
}
