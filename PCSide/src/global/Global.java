package global;

import java.util.Date;
import java.util.HashMap;

import view.LogFrame;

/**
 * This class is for providing functions that we will use through out the
 * project
 * 
 * @author Sadek Jabr.
 */
public class Global {
	public static HashMap<String, String> CONFIGURATION = new HashMap<String, String>();
	public static final String MODE = "MODE";

	/**
	 * This will print the inputed object and next to it the time it is printed
	 * 
	 * @param obj
	 *            , the object to be printed
	 */
	public static void log(Object obj) {
		Date date = new Date();
		System.out.println("[" + date + "] " + obj);
		LogFrame.log("[" + date + "] " + obj);
	}

	/**
	 * This will print the inputed object <b>as error</b> and next to it the
	 * time it is printed
	 * 
	 * @param obj
	 *            , the object to be printed
	 */
	public static void logError(Object obj) {
		Date date = new Date();
		System.err.println("[" + date + "] " + obj);
		LogFrame.log("[" + date + "] " + obj);
	}

	/**
	 * 
	 * @return
	 */
	public static int getRows() {
		int value = 4;
		try {
			value = Integer.parseInt(CONFIGURATION.get("rows"));
		} catch (Exception e) {
		}
		return value;
	}

	/**
	 * 
	 * @return
	 */
	public static int getColumns() {
		int value = 4;
		try {
			value = Integer.parseInt(CONFIGURATION.get("columns"));
		} catch (Exception e) {
		}
		return value;
	}

	/**
	 * 
	 * @return
	 */
	public static String getScreenFormat() {
		return CONFIGURATION.get("screenFormat");
	}

	/**
	 * 
	 * @return
	 */
	public static int getPort() {
		try {
			return Integer.parseInt(CONFIGURATION.get("port"));
		} catch (Exception e) {
			return 6789;
		}
	}

	/**
	 * 
	 * @return
	 */
	public static String getIP() {
		return CONFIGURATION.get("IP");
	}

	/**
	 * 
	 * @return
	 */
	public static String getExternalIP() {
		return CONFIGURATION.get("ExternalIP");
	}

	/**
	 * 
	 * @return
	 */
	public static void setRows(int rows) {
		CONFIGURATION.put("rows", rows + "");
	}

	/**
	 * 
	 * @return
	 */
	public static void setColumns(int columns) {
		CONFIGURATION.put("columns", columns + "");
	}

	/**
	 * 
	 * @return
	 */
	public static void setScreenFormat(String screenFormat) {
		CONFIGURATION.put("screenFormat", screenFormat);
	}

	/**
	 * 
	 * @return
	 */
	public static void setPort(int port) {
		CONFIGURATION.put("port", port + "");
	}

}
