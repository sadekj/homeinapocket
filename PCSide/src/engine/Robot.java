package engine;

import global.Global;

import java.awt.AWTException;
import java.awt.Rectangle;
import java.awt.event.InputEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Scanner;
import java.util.StringTokenizer;

import javax.imageio.ImageIO;

import network.ConnectionHandler;
import view.FTPLogFrame;

public class Robot extends java.awt.Robot {

	public static HashMap<Integer, String> commands;
	private static Robot robot;
	static int x = 0, y = 0;
	public static boolean[] changes;
	public static String strChanges;
	static boolean changed = false;
	public static File[] blocksFiles;
	public static BufferedImage[] blocks;
	static int blockWidth, blockHeight;
	public static boolean firstRun = true;

	public Robot() throws AWTException, FileNotFoundException {
		super();
		init();
	}

	public static Robot getRobot() throws FileNotFoundException, AWTException {
		if (robot == null)
			robot = new Robot();
		return robot;
	}

	public BufferedImage takeScreenShot(int x, int y, int width, int height) {
		return createScreenCapture(new Rectangle(x, y, width, height));
	}

	public void init() throws FileNotFoundException {
		File file = new File("CommandsConfig");
		Scanner s = new Scanner(file);
		commands = new HashMap<Integer, String>();
		while (s.hasNext()) {
			String line = s.nextLine();
			String[] valueMethod = line.split("=");
			int methodID = Integer.parseInt(valueMethod[0]);
			commands.put(methodID, valueMethod[1]);
			if (valueMethod[1].equals("engine.Robot.custom")) {
				while (!line.endsWith("=end")) {
					line = s.nextLine();
				}
			}
		}
		s.close();
		// initial global configuration
		file = new File("GlobalConfiguration.conf");
		s = new Scanner(file);
		String line = "";
		while (s.hasNext()) {
			line = s.nextLine();
			String[] keyAndValue = line.split("=");
			Global.CONFIGURATION.put(keyAndValue[0], keyAndValue[1]);
		}
		blocksFiles = new File[Global.getRows() * Global.getColumns()];
		blocks = new BufferedImage[Global.getRows() * Global.getColumns()];
		changes = new boolean[Global.getRows() * Global.getColumns()];
		s.close();
	}

	public static BufferedImage[] imgToBlocks(BufferedImage bim)
			throws IOException {
		if (robot == null)
			return null;
		/*
		 * Dividing (Segmenting) the screenshot into 16 equally blocks: set
		 * number of blocks to which we want to split a the screenshot, and that
		 * is done by multiply rows by columns
		 * 
		 * then set the width & height of each block to have equally sized
		 * blocks and then declare an array of blocks (imgs[])
		 */

		blockWidth = bim.getWidth() / Global.getColumns();
		blockHeight = bim.getHeight() / Global.getRows();
		BufferedImage blocks2[] = new BufferedImage[blocks.length];
		if (!firstRun) {
			System.arraycopy(blocks, 0, blocks2, 0, blocks.length);
		}
		int x = 0, y = 0; // (x,y) coordinates of the left corner of the block

		// store blocks in imgs[]
		int i = 0;
		for (y = 0; y < Global.getRows() * blockHeight; y += blockHeight)
			for (x = 0; x < Global.getColumns() * blockWidth; x += blockWidth)
				blocks[i++] = bim.getSubimage(x, y, blockWidth, blockHeight);
		if (!firstRun)
			getDifferences(blocks2, Robot.blocks);
		return blocks;

	}

	public static void storeBlocks(BufferedImage[] blocks, String prefix)
			throws IOException {
		// store the blocks in Screen Format image files
		for (int i = 0; i < blocks.length; i++) {
			ImageIO.write(blocks[i], Global.getScreenFormat(), new File(prefix
					+ i + "." + Global.getScreenFormat().toLowerCase()));
		}

	}

	public static boolean getDifferences(BufferedImage[] bufferedImages,
			BufferedImage[] bufferedImages2) throws IOException {
		/*
		 * find differences between the old screenshot & the new one and saving
		 * numbers of blocks changed in the string changes seperated by "-" and
		 * finally return the result as an array contains numbers of blocks
		 * changed
		 */

		/*
		 * comparison between blocks depends on the size of blocks
		 */
		changed = false;
		strChanges = Global.getRows() + "," + Global.getColumns() + ","
				+ blockWidth + "," + blockHeight;
		for (int blockIndex = 0; blockIndex < bufferedImages.length; blockIndex++) {
			if (!bufferedImagesEqual(bufferedImages[blockIndex],
					bufferedImages2[blockIndex])) {
				changes[blockIndex] = true;
				strChanges += "," + blockIndex;
				changed = true;
			}
		}
		return changed;

	}

	private static boolean bufferedImagesEqual(BufferedImage img1,
			BufferedImage img2) {
		if (img1.getWidth() == img2.getWidth()
				&& img1.getHeight() == img2.getHeight()) {
			for (int x = 0; x < img1.getWidth(); x++) {
				for (int y = 0; y < img1.getHeight(); y++) {
					if (img1.getRGB(x, y) != img2.getRGB(x, y))
						return false;
				}
			}
		} else {
			return false;
		}
		return true;
	}

	/**
	 * 
	 * @param command
	 *            should be a string in the following format <command's
	 *            ID>:<command Arguments></br> The colon ':' is the default
	 *            separator ',' is the default separator between command
	 *            arguments
	 */
	public void doCommand(String command) {
		try {
			if (command == null)
				return;
			StringTokenizer strT = new StringTokenizer(command, ":");
			int commandIndex = Integer.parseInt(strT.nextToken());
			String classAndMethod = commands.get(commandIndex);
			StringTokenizer tokens = new StringTokenizer(classAndMethod, ".");
			String className = "";
			int length = tokens.countTokens();
			for (int tokenIndex = 0; tokenIndex < length - 1; tokenIndex++) {
				className += tokens.nextToken();
				if (tokenIndex != length - 2)
					className += ".";
			}
			String methodName = tokens.nextToken();
			Class<?> cl = Class.forName(className);
			String methodArguments = "";
			if (strT.hasMoreTokens())
				methodArguments = strT.nextToken();
			cl.getMethod(methodName, String.class)
					.invoke(null, methodArguments);
		} catch (Exception e) {
			e.printStackTrace();
			Global.logError("Error while executing command (" + e.getMessage()
					+ " Exception");
		}

	}

	// 1
	public static void moveMouse(String command) {
		if (robot == null)
			return;

		Global.log(command);
		String[] params = command.split(",");

		int x = Integer.parseInt(params[0]);
		int y = Integer.parseInt(params[1]);

		if (x != 0 && y != 0)
			robot.mouseMove(x, y);

		Robot.x = x;
		Robot.y = y;

	}

	// 2

	public static void leftClickMouse(String command) {
		moveMouse(command);

		robot.mousePress(InputEvent.BUTTON1_MASK);
		robot.mouseRelease(InputEvent.BUTTON1_MASK);

	}

	// 3

	public static void leftClickMouseWithoutMove(String Command) {

		robot.mousePress(InputEvent.BUTTON1_MASK);
		robot.mouseRelease(InputEvent.BUTTON1_MASK);

	}

	// 4

	public static void rightClickMouse(String command) {
		moveMouse(command);

		robot.mousePress(InputEvent.BUTTON3_MASK);
		robot.mouseRelease(InputEvent.BUTTON3_MASK);

	}

	// 5
	/**
	 * 
	 * @param command
	 *            is a number of notches to scroll, negative numbers means
	 *            movement direction is up
	 */
	public static void rollMouseWheel(String command) {
		int notches = 0;
		try {
			notches = Integer.parseInt(command);
		} catch (Exception e) {
			Global.log("engine.Robot.rollMouseWheel: Error while parsing Notches");
		}
		robot.mouseWheel(notches);
	}

	// 6
	/**
	 * method to use keyboard keys
	 * 
	 * 
	 * @param command
	 *            is in the form (( click_mode,key_code ))
	 * 
	 *            click mode : p = press OR c = click OR r = release
	 * 
	 *            for example (( p,18 )) stands for press alt button
	 * 
	 */

	public static void keyboardKeys(String command) {
		String[] keyTDo = command.split("=");
		if (keyTDo[0].equals("p") || keyTDo[0].equals("c")) {
			robot.keyPress(Integer.parseInt(keyTDo[1]));
		}
		if (keyTDo[0].equals("c") || keyTDo[0].equals("r")) {
			robot.keyRelease(Integer.parseInt(keyTDo[1]));
		}
	}

	// 7
	public static void updateGlobalConfig(String command) {
		String[] keyAndValue = command.split("=");
		Global.CONFIGURATION.put(keyAndValue[0], keyAndValue[1]);
		if (keyAndValue[0].equals("rows") || keyAndValue[0].equals("columns")) {
			blocksFiles = new File[Global.getRows() * Global.getColumns()];
			blocks = new BufferedImage[Global.getRows() * Global.getColumns()];
			changes = new boolean[Global.getRows() * Global.getColumns()];
		}
	}

	// 8
	public static void custom(String command) throws IOException {
		File file = new File("CommandsConfig");
		Scanner s = new Scanner(file);
		String line;
		do {
			line = s.nextLine();
		} while (!(line.startsWith(command + "=") && line
				.endsWith("=engine.Robot.custom")));
		line = s.nextLine();
		while (!line.equals(command + "=end")) {
			robot.doCommand(line);
			line = s.nextLine();
		}
		s.close();
	}

	// 9

	/**
	 * 
	 * @param command
	 *            start cursor x , start cursor x , end cursor x , end cursor y
	 * 
	 * 
	 */
	public static void dragMouse(String command) {
		String[] arg = command.split(",");

		moveMouse(arg[0] + "," + arg[1]);

		robot.mousePress(InputEvent.BUTTON1_MASK);

		moveMouse(arg[2] + "," + arg[3]);
		robot.mouseRelease(InputEvent.BUTTON1_MASK);

	}

	// 10
	/**
	 * 
	 * @param command
	 */
	public static void ftpReceiveFile(String command) {
		try {
			ConnectionHandler.getConnectionHandler().ftpReceiveFile();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void ftpSendFile() {
		try {
			ConnectionHandler.getConnectionHandler().ftpSendFile(
					FTPLogFrame.getftpLogFrame().getLocationToBeSavedin(),
					FTPLogFrame.getftpLogFrame().getFileLocation(),
					FTPLogFrame.getftpLogFrame().getFileName());
		} catch (Exception e) {
		}
	}

}
