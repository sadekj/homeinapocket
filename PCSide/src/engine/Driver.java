package engine;

import global.Global;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.io.File;

import javax.imageio.ImageIO;
import javax.swing.JOptionPane;

import view.MainFrame;
import network.ConnectionHandler;

public class Driver {
	public static void main(String[] args) throws Exception {
		new MainFrame();
		Robot robot = Robot.getRobot();
		ConnectionHandler conH = new ConnectionHandler(Global.getPort());
		Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
		boolean isScreenAllowed = false;
		while (true) {
			try {
				Global.log("Waiting a connection at: " + conH.getIp() + ":"
						+ conH.getPort());
				conH.waitConnection();
				String command = conH.receiveCommand();
				if (command.startsWith("password=")) {
					String passowrd = command.split("=")[1];
					if (Global.CONFIGURATION.get("password").equals(passowrd))
						Global.CONFIGURATION.put(Global.MODE, "password");
					else
						conH.terminateConnection();
				} else if (command.equals("POLITE")) {
					int reply = JOptionPane
							.showConfirmDialog(
									null,
									"A client is asking to remote control your Computer",
									"Home In a Pocket",
									JOptionPane.YES_NO_OPTION);
					if (reply == JOptionPane.YES_OPTION)
						Global.CONFIGURATION.put(Global.MODE, "polite");
					else
						conH.terminateConnection();

				} else {
					conH.terminateConnection();
				}

				if (conH.isConnected()) {
					command = conH.receiveCommand();
					if (command.startsWith("screen=")) {
						isScreenAllowed = Boolean.parseBoolean(command
								.split("=")[1]);
					} else
						conH.terminateConnection();
					if (isScreenAllowed) {
						ImageIO.write(
								Robot.getRobot().takeScreenShot(0, 0,
										(int) screen.getWidth(),
										(int) screen.getHeight()), "GIF",
								new File("screen.gif"));
						conH.sendFile("screen.gif");
					}
				}
				while (conH.isConnected()) {
					if (isScreenAllowed) {
						conH.sendScreen(Robot.imgToBlocks(robot.takeScreenShot(
								0, 0, (int) screen.getWidth(),
								(int) screen.getHeight())), Robot.changes);
						Robot.firstRun = false;
					} else {
						while (!command.contains(":"))
							command = conH.receiveCommand();
						Robot.getRobot().doCommand(command);
					}
				}
			} catch (Exception e) {
				Global.logError("Something went wrong, Disconnecting...");
				conH.terminateConnection();
				e.printStackTrace();
			}
			isScreenAllowed = false;
		}

	}
}
