package engine;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;

import javax.swing.JOptionPane;

import network.ftp.FTPClient;
import view.ConfigurationFrame;
import view.FTPLogFrame;
import view.LogFrame;

public class ButtonsActionListener implements ActionListener {

	@Override
	public void actionPerformed(ActionEvent e) {
		try {
			String btnsText = e.getActionCommand();
			switch (btnsText) {
			case "LOG":
				LogFrame.getLogFrame().setVisible(true);
				break;
			case "Save LOG to File":
				File file = new File("Log.txt");
				PrintWriter printWriter;
				try {
					printWriter = new PrintWriter(file);
					printWriter.append(LogFrame.getLogFrame().getLog());
					printWriter.flush();
					printWriter.close();
					JOptionPane.showMessageDialog(null, "Log Saved!");
				} catch (FileNotFoundException e1) {
					JOptionPane.showMessageDialog(null, "Failed Saving Log!");
				}
				break;
			case "Customed Commands":
				break;
			case "HELP":
				break;
			case "FTP":
				FTPLogFrame.getftpLogFrame();
				FTPClient.getFtpClient();
				FTPLogFrame.getftpLogFrame().setVisible(true);
				break;
			case "Set Password":
				break;
			case "Configuration File":
				ConfigurationFrame.getConfigurationFrame().setVisible(true);
				ConfigurationFrame.getConfigurationFrame().updateValues();
				break;
			case "Exit":
				break;
			case "Save Configurations":
				ConfigurationFrame.getConfigurationFrame().saveValues();
				break;
			case "Send File":
				FTPClient.getFtpClient().sendFile();
				break;
			default:
			}
		} catch (Exception e1) {
			JOptionPane
					.showMessageDialog(
							null,
							"Error, a group of limited thinking programers did not think through this error");
			e1.printStackTrace();
		}
	}

}
