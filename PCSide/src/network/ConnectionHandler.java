package network;

import engine.Robot;
import global.Global;

import java.awt.AWTException;
import java.awt.image.BufferedImage;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.URL;
import java.util.List;

import javax.imageio.ImageIO;
import javax.swing.JOptionPane;

import network.ftp.FTPClient;
import network.ftp.FTPServer;

import org.omg.CORBA.NameValuePair;
import org.teleal.cling.UpnpService;
import org.teleal.cling.UpnpServiceImpl;
import org.teleal.cling.support.igd.PortMappingListener;
import org.teleal.cling.support.model.PortMapping;

import view.IPPortPanel;

/**
 * This class provides all the needed network functionalities
 * 
 * @author Sadek Jabr
 * 
 */
public class ConnectionHandler {
	BufferedReader inFromClient;
	DataOutputStream outToClient;
	boolean connected = false, updateScreen = false;
	ServerSocket welcomeSocket;
	Socket connectionSocket;
	OutputStream outputStream;
	InputStream inputStream;
	FileInputStream fileInputStream;
	BufferedInputStream bufferedInputStream;
	int sentNum = 0, recievedNum = 0, port;
	String ip;
	static ConnectionHandler connectionHandler;

	/**
	 * This will create server listening on the the inputed port
	 * 
	 * @param port
	 *            , the port to listen
	 * @throws Exception
	 */
	public ConnectionHandler(int port) throws Exception {
		welcomeSocket = new ServerSocket(port);
		// Creating a socket which will be waiting users to connect with it
		ip = InetAddress.getLocalHost().getHostAddress();
		this.port = welcomeSocket.getLocalPort();
		Global.log("SERVER: Created at " + ip + ":" + port);
		Global.CONFIGURATION.put("IP", ip);
		Global.CONFIGURATION.put("ExternalIP", "No Internet Detected Yet!");
		IPPortPanel.getIpPortPanel().updateValues();
		new Thread(new InternetDetecting()).start();
		connectionHandler = this;
		// This while loop to keep the server running for ever, and keep
		// waiting
		// clients
	}

	/**
	 * This function takes a none parsed command and parses it and send it
	 * 
	 * @param command
	 *            , the command number to be executed
	 * @param params
	 *            , the list of parameters and it's values
	 * @return boolean, true if success sent, false otherwise
	 * @author Sadek Jabr
	 */
	public boolean sendCommand(int command, List<? extends NameValuePair> params) {
		if (!connected || outToClient == null)
			return false;

		String strCommand = "";
		for (NameValuePair pair : params) {
			strCommand += pair.id + "=" + pair.value;
		}
		return sendCommand(strCommand);
	}

	public static ConnectionHandler getConnectionHandler() {
		if (connectionHandler == null)
			try {
				connectionHandler = new ConnectionHandler(Global.getPort());
			} catch (Exception e) {
				e.printStackTrace();
			}
		return connectionHandler;
	}

	/**
	 * 
	 * @return the port
	 */
	public int getPort() {
		return port;
	}

	/**
	 * 
	 * @return the IP address
	 */
	public String getIp() {
		return ip;
	}

	/**
	 * This function will send a giving message as a new line
	 * 
	 * @param strCommand
	 *            , the message to send
	 * @return boolean, true if success sent, false otherwise
	 * @author Sadek Jabr
	 */
	public boolean sendCommand(String strCommand) {
		if (connected || outToClient != null) {
			try {
				sentNum++;
				Global.log(sentNum + " " + "Sending command:" + strCommand);
				outToClient.writeBytes(strCommand + "\n");
				return true;
			} catch (IOException e) {
				Global.logError("Faild sending command " + strCommand);
			}
		}
		return false;
	}

	/**
	 * This function returns one line message from the stream or null if
	 * something is wrong, and will wait for a message if no message is there
	 * 
	 * @return String, the message from the stream
	 * @author Sadek Jabr
	 */
	public String receiveCommand() {
		if (connected || inFromClient != null) {
			try {
				String command = inFromClient.readLine();
				recievedNum++;
				Global.log(recievedNum + " " + "Received Command:" + command);
				if (command.equals("terminate=true"))
					terminateConnection();
				return command;
			} catch (IOException e) {
				Global.logError("Faild Receiving data");
			}
		}
		return null;
	}

	/**
	 * This function will accept and prepare the streams for an incoming
	 * connection
	 * 
	 * @return boolean, true if successfully connected, false otherwise
	 * @author Sadek Jabr
	 */
	public boolean waitConnection() {
		if (!connected) {
			try {
				Global.log("SERVER: Waiting for Client...");
				// Waiting until a client sends a connection
				connectionSocket = welcomeSocket.accept();
				inFromClient = new BufferedReader(new InputStreamReader(
						connectionSocket.getInputStream()));
				outToClient = new DataOutputStream(
						connectionSocket.getOutputStream());
				outputStream = connectionSocket.getOutputStream();
				inputStream = connectionSocket.getInputStream();
				// Creating a Data Stream to get the client's data stream
				Global.log("SERVER: Client connected at "
						+ connectionSocket.getLocalAddress() + ":"
						+ connectionSocket.getLocalPort());
				new FTPServer(connectionSocket);
				new FTPClient(connectionSocket);
				connected = true;
			} catch (Exception e) {
				Global.logError("Faild Accepting connection");
				connected = false;
				e.printStackTrace();
			}
		}
		return connected;
	}

	/**
	 * This function will terminate an active connection, and make the server
	 * available to accept a new connection
	 * 
	 * @return boolean, true if disconnected, false otherwise
	 * @author Sadek Jabr
	 */
	public boolean terminateConnection() {
		sendCommand("terminate=true");
		connected = false;
		try {
			if (outputStream != null)
				outputStream.close();
		} catch (IOException e) {
			Global.logError("Failed closing streams while terminating connection");
		}
		return !connected;
	}

	/**
	 * This function will shutdown the server
	 * 
	 * @return boolean, true if server turned off successfully, false otherwise
	 */
	public boolean shutdown() {
		try {
			inFromClient.close();
			outToClient.close();
			welcomeSocket.close();
			return true;
		} catch (IOException e) {
			Global.logError("Failed turnning off the server");
		}
		return false;
	}

	/**
	 * This function will send a bufferedImage
	 * 
	 * @param bi
	 *            , the BufferedImage object to send
	 * @return boolean, true if successfuly sent, false otherwise
	 * @throws Exception
	 */
	public void sendScreen(BufferedImage[] bi, boolean[] changes)
			throws Exception {
		sendCommand(Robot.strChanges);
		Global.log("Expecting READY1");
		expectingCommand("READY1");
		if (Robot.strChanges == null || Robot.strChanges.equals("null"))
			return;
		for (int i = 0; i < bi.length; i++)
			if (changes[i]) {
				Global.log("Sendling Block[" + i + "]");
				sendScreenBlock(bi[i], i);
				changes[i] = false;
				// Global.log("Expecting READY2");
				// expectingCommand("READY2");
			}
		// Global.log("Expecting READY3");
		// expectingCommand("READY3");
		// sendCommand("END");
	}

	/**
	 * This function will make the thread wait until the expectedMsg is
	 * received, while in the same time, any other message will be executed
	 * 
	 * @param expectedMsg
	 *            , the expected message to recieve
	 */
	public void expectingCommand(String expectedMsg) {
		String feedback = "";
		feedback = receiveCommand();
		while (!feedback.equals(expectedMsg)) {
			if (feedback.contains(":"))
				try {
					Robot.getRobot().doCommand(feedback);
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				} catch (AWTException e) {
					e.printStackTrace();
				}
			feedback = receiveCommand();
		}
	}

	/**
	 * This function will send the BufferedImage block, it's index and in the
	 * format set in the global configuration, such as screen7.gif
	 * 
	 * @param bufferedImage
	 * @param bufferedImageIndex
	 * @throws Exception
	 */
	public void sendScreenBlock(BufferedImage bufferedImage,
			int bufferedImageIndex) throws Exception {
		ImageIO.write(bufferedImage, Global.getScreenFormat(), new File(
				"screen" + bufferedImageIndex + "."
						+ Global.getScreenFormat().toLowerCase()));
		sendFile("screen" + bufferedImageIndex + "."
				+ Global.getScreenFormat().toLowerCase());
	}

	/**
	 * Connection status
	 * 
	 * @return true if connected, false otherwise
	 */
	public boolean isConnected() {
		return connected;
	}

	/**
	 * Send a file to a connected socket.
	 * <p>
	 * First it sends file size in 4 bytes then the file's content.
	 * </p>
	 * <p>
	 * Note: File size is limited to a 32bit signed integer, 2GB
	 * </p>
	 * 
	 * @param os
	 *            OutputStream of the connected socket
	 * @param fileName
	 *            The complete file's path of the image to send
	 * @throws Exception
	 * @see {@link receiveFile} for an example how to receive file at other
	 *      side.
	 * 
	 */
	public void sendFile(String fileName) throws Exception {
		Global.log("Sending file ....");
		// File to send
		File myFile = new File(fileName);
		int fSize = (int) myFile.length();
		if (fSize < myFile.length()) {
			System.out.println("File is too big'");
			throw new IOException("File is too big.");
		}

		// Send the file's size
		byte[] bSize = new byte[4];
		bSize[0] = (byte) ((fSize & 0xff000000) >> 24);
		bSize[1] = (byte) ((fSize & 0x00ff0000) >> 16);
		bSize[2] = (byte) ((fSize & 0x0000ff00) >> 8);
		bSize[3] = (byte) (fSize & 0x000000ff);
		// 4 bytes containing the file size
		outputStream.write(bSize, 0, 4);

		// In case of memory limitations set this to false
		boolean noMemoryLimitation = false;

		FileInputStream fileInputStream = new FileInputStream(myFile);
		BufferedInputStream bufferedInputStream = new BufferedInputStream(
				fileInputStream);
		try {
			if (noMemoryLimitation) {
				// Use to send the whole file in one chunk
				byte[] outBuffer = new byte[fSize];
				int bRead = bufferedInputStream.read(outBuffer, 0,
						outBuffer.length);
				outputStream.write(outBuffer, 0, bRead);
			} else {
				// Use to send in a small buffer, several chunks
				int bRead = 0;
				byte[] outBuffer = new byte[8 * 1024];
				while ((bRead = bufferedInputStream.read(outBuffer, 0,
						outBuffer.length)) > 0) {
					outputStream.write(outBuffer, 0, bRead);
				}
			}
			// outputStream.flush();
		} finally {
			// bufferedInputStream.close();
			if (fileInputStream != null)
				fileInputStream.close();
			if (bufferedInputStream != null)
				bufferedInputStream.close();
			Global.log("File Sent");
		}
	}

	public void ftpSendFile(String locationToBeSavedin, String FileLocation,
			String fileName) throws Exception {
		File f = new File(locationToBeSavedin + fileName);
		if (!f.exists()) {
			sendCommand("File Not Found");
			return;
		} else {
			sendCommand(locationToBeSavedin);
			sendCommand(fileName);
			String command = receiveCommand();
			if (command.equals("ftp:fileExsits")) {
				int reply = JOptionPane.showConfirmDialog(null,
						"Override exsiting file?", "FTP overrifing",
						JOptionPane.YES_NO_OPTION);
				if (reply == JOptionPane.YES_OPTION)
					sendCommand("ftp:yes");
				else {
					sendCommand("ftp:no");
					return;
				}
			} else {
				sendFile(fileName);
			}
		}
	}

	public File ftpReceiveFile() throws Exception {
		String fileLocation = receiveCommand(), fileName = receiveCommand();
		String fileInES = fileLocation + fileName;
		File file = new File(fileInES);
		if (file.exists()) {
			sendCommand("ftp:fileExsits");
			// asking for overriding
			String command = receiveCommand();
			if (command.equals("ftp:no"))
				return null;
		} else
			sendCommand("ftp:fileNotExsits");
		return receiveFile(fileInES);
	}

	public File receiveFile(String fileInES) throws Exception {
		// read 4 bytes containing the file size
		byte[] bSize = new byte[4];
		int offset = 0;
		while (offset < bSize.length) {
			int bRead = inputStream.read(bSize, offset, bSize.length - offset);
			offset += bRead;
		}
		// Convert the 4 bytes to an int
		int fileSize;
		fileSize = (int) (bSize[0] & 0xff) << 24
				| (int) (bSize[1] & 0xff) << 16 | (int) (bSize[2] & 0xff) << 8
				| (int) (bSize[3] & 0xff);

		// buffer to read from the socket
		// 8k buffer is good enough
		byte[] data = new byte[8 * 1024];

		int bToRead;
		FileOutputStream fos = new FileOutputStream(fileInES);
		BufferedOutputStream bos = new BufferedOutputStream(fos);
		while (fileSize > 0) {
			// make sure not to read more bytes than filesize
			if (fileSize > data.length)
				bToRead = data.length;
			else
				bToRead = fileSize;
			int bytesRead = inputStream.read(data, 0, bToRead);
			if (bytesRead > 0) {
				bos.write(data, 0, bytesRead);
				fileSize -= bytesRead;
			}
		}
		bos.close();

		// Convert the received image to a Bitmap
		// If you do not want to return a bitmap comment/delete the folowing
		// lines
		// and make the function to return void or whatever you prefer.
		return new File(fileInES);
	}

	class InternetDetecting implements Runnable {

		@Override
		public void run() {
			try {
				URL whatismyip = new URL("http://icanhazip.com/");
				BufferedReader in = new BufferedReader(new InputStreamReader(
						whatismyip.openStream()));
				String ip = in.readLine(); // you get the External IP as a
											// String
				Global.CONFIGURATION.put("ExternalIP", ip);
				// PortMapping desiredMapping = new PortMapping(port, ip,
				// PortMapping.Protocol.TCP, "HIP Port Mapping");
				//
				// UpnpService upnpService = new UpnpServiceImpl(new
				// PortMappingListener(
				// desiredMapping));
				//
				// upnpService.getControlPoint().search();
			} catch (Exception e) {
				Global.CONFIGURATION.put("ExternalIP",
						"Failed detecting Internet");
			}
			IPPortPanel.getIpPortPanel().updateValues();
		}
	}
}
