package network.ftp;

//FTP Client

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.net.Socket;

import network.ConnectionHandler;
import view.FTPLogFrame;

public class FTPClient {
	static FTPClient ftpClient;
	transferfileClient transferfileClient;

	public FTPClient(Socket socket) {
		transferfileClient = new transferfileClient(socket);
		ftpClient = this;
		// t.displayMenu();
	}

	public static FTPClient getFtpClient() throws Exception {
		if (ftpClient == null)
			throw new NullPointerException(
					"FTPClient has to be first instatioated");
		else
			return ftpClient;
	}

	public boolean sendFile() {
		try {
			transferfileClient.SendFile();
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	public boolean receiveFile() {
		try {
			transferfileClient.ReceiveFile();
		} catch (Exception e) {
			return false;
		}
		return true;
	}
}

class transferfileClient {
	Socket ClientSoc;

	DataInputStream din;
	DataOutputStream dout;
	BufferedReader br;

	String command;

	transferfileClient(Socket soc) {
		try {
			ClientSoc = soc;
			din = new DataInputStream(ClientSoc.getInputStream());
			dout = new DataOutputStream(ClientSoc.getOutputStream());
			br = new BufferedReader(new InputStreamReader(System.in));
		} catch (Exception ex) {
		}
	}

	void SendFile() throws Exception {
		sendFTPCommand("SEND");
		String filename;
		FTPLogFrame.log("Enter File Name :");
		filename = FTPLogFrame.getftpLogFrame().getPath();

		File f = new File(filename);
		if (!f.exists()) {
			FTPLogFrame.log("File not Exists...");
			sendFTPCommand(
					"File not found");
			return;
		}

		sendFTPCommand(filename);

		String msgFromServer = getCommand();
		if (msgFromServer.compareTo("File Already Exists") == 0) {
			if (FTPLogFrame.getftpLogFrame().FileAlreadyExists()) {
				sendFTPCommand("Y");
			} else {
				sendFTPCommand("N");
				return;
			}
		}

		FTPLogFrame.log("Sending File ...");
		FileInputStream fin = new FileInputStream(f);
		int ch;
		do {
			ch = fin.read();
			sendFTPCommand(
					String.valueOf(ch));
		} while (ch != -1);
		fin.close();
		FTPLogFrame.log(getCommand());

	}

	void ReceiveFile() throws Exception {
		sendFTPCommand("GET");
		String fileName;
		FTPLogFrame.log("Enter File Name :");
		fileName = br.readLine();
		sendFTPCommand(fileName);
		String msgFromServer = getCommand();

		if (msgFromServer.compareTo("File Not Found") == 0) {
			FTPLogFrame.log("File not found on Server ...");
			return;
		} else if (msgFromServer.compareTo("READY") == 0) {
			FTPLogFrame.log("Receiving File ...");
			File f = new File(fileName);
			if (f.exists()) {
				String Option;
				System.out
						.println("File Already Exists. Want to OverWrite (Y/N) ?");
				Option = br.readLine();
				if (Option == "N") {
					dout.flush();
					return;
				}
			}
			FileOutputStream fout = new FileOutputStream(f);
			int ch;
			String temp;
			do {
				temp = getCommand();
				ch = Integer.parseInt(temp);
				if (ch != -1) {
					fout.write(ch);
				}
			} while (ch != -1);
			fout.close();
			FTPLogFrame.log(getCommand());

		}
	}

	public void setCommand(String command) {
		this.command = command;
	}

	public String getCommand() {
		while (command == null)
			continue;
		String tempCommand = command;
		command = null;
		return tempCommand;
	}

	public void sendFTPCommand(String command) {
		ConnectionHandler.getConnectionHandler().sendCommand("10:" + command);
	}

	public void displayMenu() throws Exception {
		while (true) {
			FTPLogFrame.log("[ MENU ]");
			FTPLogFrame.log("1. Send File");
			FTPLogFrame.log("2. Receive File");
			FTPLogFrame.log("3. Exit");
			FTPLogFrame.log("\nEnter Choice :");
			int choice;
			choice = Integer.parseInt(br.readLine());
			if (choice == 1) {
				sendFTPCommand("SEND");
				SendFile();
			} else if (choice == 2) {
				sendFTPCommand("GET");
				ReceiveFile();
			} else {
				sendFTPCommand(
						"DISCONNECT");
				System.exit(1);
			}
		}
	}
}
