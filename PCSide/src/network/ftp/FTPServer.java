package network.ftp;

//FTP Server

import global.Global;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.net.Socket;

import network.ConnectionHandler;

public class FTPServer {
	transferfile transferfileClient;
	static FTPServer ftpServer;
	public FTPServer(Socket socket) {
		Global.log("Waiting for Connection ...");
		transferfileClient = new transferfile(socket);
	}

	public void setCommand(String command) {
		transferfileClient.setCommand(command);
	}

	public String getCommand() {
		return transferfileClient.getCommand();
	}

	public static FTPServer getFtpServer() {
		if (ftpServer == null)
			throw new NullPointerException(
					"FTPServer has to be first instatioated");
		else
			return ftpServer;
	}
}

class transferfile extends Thread {
	Socket ClientSoc;
	String command;
	DataInputStream din;
	DataOutputStream dout;

	transferfile(Socket soc) {
		try {
			ClientSoc = soc;
			din = new DataInputStream(ClientSoc.getInputStream());
			dout = new DataOutputStream(ClientSoc.getOutputStream());
			Global.log("FTP Client Connected ...");
			start();

		} catch (Exception ex) {
		}
	}

	void SendFile() throws Exception {
		String filename = getCommand();
		File f = new File(filename);
		if (!f.exists()) {
			sendFTPCommand("File Not Found");
			return;
		} else {
			sendFTPCommand("READY");
			FileInputStream fin = new FileInputStream(f);
			int ch;
			do {
				ch = fin.read();
				sendFTPCommand(String.valueOf(ch));
			} while (ch != -1);
			fin.close();
			sendFTPCommand("File Receive Successfully");
		}
	}

	void ReceiveFile() throws Exception {
		String filename = getCommand();
		if (filename.compareTo("File not found") == 0) {
			return;
		}
		File f = new File(filename);
		String option;

		if (f.exists()) {
			sendFTPCommand("File Already Exists");
			option = getCommand();
		} else {
			sendFTPCommand("SendFile");
			option = "Y";
		}

		if (option.compareTo("Y") == 0) {
			FileOutputStream fout = new FileOutputStream(f);
			int ch;
			String temp;
			do {
				temp = getCommand();
				ch = Integer.parseInt(temp);
				if (ch != -1) {
					fout.write(ch);
				}
			} while (ch != -1);
			fout.close();
			sendFTPCommand("File Send Successfully");
		} else {
			return;
		}

	}

	public void setCommand(String command) {
		this.command = command;
	}

	public String getCommand() {
		while (command == null)
			continue;
		String tempCommand = command;
		command = null;
		return tempCommand;
	}
	public void sendFTPCommand(String command) {
		ConnectionHandler.getConnectionHandler().sendCommand("10:" + command);
	}

	public void run() {
		while (true) {
			try {
				Global.log("Waiting for Command ...");
				String Command = getCommand();
				if (Command.compareTo("GET") == 0) {
					Global.log("\tGET Command Received ...");
					SendFile();
					continue;
				} else if (Command.compareTo("SEND") == 0) {
					Global.log("\tSEND Command Receiced ...");
					ReceiveFile();
					continue;
				} else if (Command.compareTo("DISCONNECT") == 0) {
					Global.log("\tDisconnect Command Received ...");
					System.exit(1);
				}
			} catch (Exception ex) {
			}
		}
	}
}