# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

Home In a Pocket is a software that is capable of letting you control your PC from a far distance whether through your Android device, iOS device or another computer. 

This repository has 4 projects, a 2 JAVA projects one is the server, and the other is client. the other two projects are iOS and android

### How do I get set up? ###

Clone import to eclipse ADT & XCode, run it , enjoy

The projects comes with a file called GlobalConfiguration.txt where there you can modify some configuration. There is also another file called CommandsConfig which has the configuration of the built-in functionalities, and to create the custom functionalities.
### For Developers ###
Step 1: Download HIP Library
Step 2:Add it as a library to your project.
Step 3:Create your own packages and classes.
Step 4:Create your static functions with only one argument of type String

	public static void myCommand(String input){
	// YOUR lines of code goes here
	}

Step 5:Add your command to the CommandsConfig file and give it an ID number
	11=mypackage.MyClass.myCommand

### For Non-Developers ###
Non Developers can create their owne custom function by calling functions created by other developers to be called in specefic order and arguments, when the system receives the ID number of the custom function.
	8=engine.Robot.custom
	1:20,20
	1:30,20
	1:30,30
	1:30,40
	1:40,40
	1:50,40
	1:50,50
	1:50,60
	1:60,60
	8=end



### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact