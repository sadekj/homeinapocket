//
//  AppDelegate.h
//  Home In a Pocket
//
//  Created by Sadek Jabr on 4/12/14.
//  Copyright (c) 2014 Sadek Jabr. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
