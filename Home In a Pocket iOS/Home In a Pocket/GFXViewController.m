//
//  GFXViewController.m
//  Home In a Pocket
//
//  Created by Sadek Jabr on 4/22/14.
//  Copyright (c) 2014 Sadek Jabr. All rights reserved.
//

#import "GFXViewController.h"
#import "ViewController.h"

@interface GFXViewController ()
{
    NSUInteger chunkSize;
    NSString * lineDelimiter;
    NSInteger blockWidth;
    NSInteger blockHeight;
    BOOL start;
    CGFloat screenWidth;
    CGFloat screenHeight;
    BOOL active;
}
@end

@implementation GFXViewController
@synthesize data=_data;
@synthesize bytesRead=_bytesRead;
@synthesize screenImg=_screenImg;
@synthesize blocks=_blocks;
@synthesize changes=_changes;
@synthesize mode=_mode;
@synthesize firstTime=_firstTime;
int PASSWORD=7;
int POLITE=6;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    start = YES;
    [self.view setMultipleTouchEnabled:YES];
    
}

-(void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    if (start == YES && _firstTime) {
        active=YES;
        [self connect];
        if (_mode == POLITE) {
            [self sendCommand:@"POLITE"];
        }else if(_mode == PASSWORD){
            [self sendCommand:[NSString stringWithFormat:@"password=%@",_strPassword]];
        }
        [self sendCommand:@"screen=true"];
        [self recieveFile:screen name:@"screen"];
        timer = [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(updateScreen) userInfo:nil repeats:YES];
        start = NO;
    }else{
        active=YES;
    }
}
-(void) touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    [touches enumerateObjectsUsingBlock:^(id obj, BOOL *stop) {
        // Get a single touch and it's location
        UITouch *touch = obj;
        CGPoint touchPoint = [touch locationInView:self.view];
        int x = touchPoint.x/screenHeight*blockHeight*4;
        int y = touchPoint.y/screenWidth*blockWidth*4;
        [self sendCommand:[NSString stringWithFormat:@"1:%d,%d",x,y]];
    }];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
-(void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    active=NO;
    if([segue.identifier isEqualToString:@"ViewController"]){
        ViewController *destinationViewController = segue.destinationViewController;
        destinationViewController.outputStream =[outputStream copy];
        destinationViewController.firstTime=NO;
    }
}
- (id)initWithName:(NSString *)IP andPort:(NSString *)port
{
    self = [super initWithNibName:@"GFXViewController" bundle:nil];
    if (self) {
        _IP = [IP copy];
        _port=[port copy];
    }
    return self;
}

- (void)initNetworkCommunication {
    CFReadStreamRef readStream;
    CFWriteStreamRef writeStream;
    CFStreamCreatePairWithSocketToHost(NULL, (__bridge CFStringRef)_IP, [_port integerValue], &readStream, &writeStream);
    inputStream = (__bridge NSInputStream *)readStream;
    outputStream = (__bridge NSOutputStream *)writeStream;
    [inputStream setDelegate:self];
    [outputStream setDelegate:self];
    [inputStream scheduleInRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
    [outputStream scheduleInRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
    [inputStream open];
    [outputStream open];
}
-(IBAction)connect
{
    [self prepareEnv];
    [self initNetworkCommunication];
}
-(IBAction) terminate
{
    NSString *command  = [NSString stringWithFormat:@"4:NO INPUT"];
    [self sendCommand:command];
    [inputStream close];
    [outputStream close];
}
-(IBAction) moveMouseX:(NSInteger)x Y:(NSInteger)y
{
    NSString *command  = [NSString stringWithFormat:@"2:%d,%d", x,y];
    [self sendCommand:command];
}
-(void) sendCommand:(NSString *) command
{
    NSString *response  = [NSString stringWithFormat:@"%@\n", command];
	NSData *data = [[NSData alloc] initWithData:[response dataUsingEncoding:NSASCIIStringEncoding]];
	[outputStream write:[data bytes] maxLength:[data length]];
    
}
- (void)recieveFile:(UIImageView *) block name:(NSString *)name
{
    NSLog(@"Receiving File ....");
    uint8_t bufSize[4];
    NSInteger offset =0;
    while (offset < 4) {
        NSInteger bRead = [inputStream read:bufSize maxLength:4];
        offset+= bRead;
    }
    int fileSize;
    fileSize = (int) (bufSize[0] & 0xff) << 24 | (int) (bufSize[1] & 0xff) << 16 | (int) (bufSize[2] & 0xff) << 8 | (int) (bufSize[3] & 0xff);
    uint8_t data[8 * 1024];
    int bToRead = 0;
    NSString *basPath = [[[NSBundle mainBundle] resourcePath]stringByAppendingString:[NSString stringWithFormat:@"%@.gif",name]];
    NSOutputStream *fos =[[NSOutputStream alloc] initToFileAtPath:basPath append:NO];
    _screenImg = [UIImage imageWithContentsOfFile:basPath];
    [block setImage:_screenImg];
    [fos setDelegate:self];
    [fos scheduleInRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
    [fos open];
    
    while (fileSize>0) {
        if (fileSize>(8 * 1024)) {
            bToRead = 8 * 1024;
        }else{
            bToRead = fileSize;
        }
        NSInteger bytesRead = [inputStream read:data maxLength:bToRead];
        if (bytesRead>0) {
            [fos write:data maxLength:bToRead];
            fileSize-=bytesRead;
        }
    }
    [fos close];
    NSLog(@"File Recieved");
}
-(void) prepareEnv
{
    _blocks = [NSArray array];
    _changes = [NSArray array];
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    screenWidth = screenRect.size.height;
    screenHeight = screenRect.size.width - 20;
    for (int i=0; i<4*4; i++) {
        UIImageView *block = [[UIImageView alloc] init];
        _blocks=[_blocks arrayByAddingObject:block];
        int left = (i % 4) * (screenWidth / 4);
        int top = (i / 4) * (screenHeight / 4);
        //block.center = CGPointMake(left, top);
        block.frame = CGRectMake(left, top + 20, screenWidth / 4,screenHeight / 4);
        //block.contentMode = UIViewContentModeBottom;
        //block.clipsToBounds = YES;
        [self.view addSubview:block];
        _changes=[_changes arrayByAddingObject:[NSNumber numberWithBool:NO]];
    }
    chunkSize = 10;
    lineDelimiter = @"\n";
}
-(NSString *) recieveCommand
{
    uint8_t buffer[1];
    int len;
    NSString *line = @"";
    while (YES) {
        len = [inputStream read:buffer maxLength:sizeof(buffer)];
        NSString *t = [NSString stringWithFormat:@"%c", buffer[0]];
        //NSLog([NSString stringWithFormat:@"%@",t]);
        if([lineDelimiter isEqualToString:t])
            break;
        else
            line = [NSString stringWithFormat:@"%@%@",line,t];
    }
    //NSLog(line);
    return line;
}
-(void) receiveScreen
{
    NSString *command = [self recieveCommand];
    while ([command isEqual:[NSNull null]] || [command rangeOfString:@":"].location != NSNotFound)
        command = [self recieveCommand];
    [self sendCommand:@"READY1"];
    NSArray *tokens = [command componentsSeparatedByString:@","];
    blockWidth = [tokens[2] integerValue];
    blockHeight = [tokens[3] integerValue];
    NSInteger blockIndex;
    for (int i=4; i<[tokens count]; i++) {
        blockIndex = [tokens[i] integerValue];
        [self recieveFile:[_blocks objectAtIndex:blockIndex] name:[NSString stringWithFormat:@"screen%d",blockIndex]];
    }
}
-(void) updateScreen
{
    if(active){
        //NSLog(@"HERE3");
        [self sendCommand:@"1:0,0"];
        [self receiveScreen];
    }
}

@end
