//
//  ViewController.h
//  Home In a Pocket
//
//  Created by Sadek Jabr on 4/12/14.
//  Copyright (c) 2014 Sadek Jabr. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController{
    IBOutlet UILabel *modeValue;
    IBOutlet UIButton *btnConnect;
    IBOutlet UIButton *btnPassword;
    IBOutlet UIButton *btnPolite;
    IBOutlet UITextField *txvIP;
    IBOutlet UITextField *txvPort;
    IBOutlet UITextField *txvPassword;
    IBOutlet UITextField *txvCutomCommand;


}
@property (copy, nonatomic, readwrite) NSOutputStream *outputStream;
@property (assign, nonatomic, readwrite) BOOL firstTime;
-(IBAction)btnPassword:(id)sender;
-(IBAction)btnPolite:(id)sender;
-(IBAction)btnSendCustomCommand:(id)sender;

@end
