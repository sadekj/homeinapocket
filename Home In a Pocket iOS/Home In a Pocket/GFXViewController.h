//
//  GFXViewController.h
//  Home In a Pocket
//
//  Created by Sadek Jabr on 4/22/14.
//  Copyright (c) 2014 Sadek Jabr. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GFXViewController : UIViewController <NSStreamDelegate>
{
    IBOutlet UILabel *testLabel;
    IBOutlet UIImageView *screen;
    NSInputStream *inputStream;
    NSOutputStream *outputStream;
    NSTimer *timer;
}
@property (copy, nonatomic, readwrite) NSString *IP;
@property (copy, nonatomic, readwrite) NSString *port;
@property (copy, nonatomic, readwrite) NSString *strPassword;
@property (assign, nonatomic, readwrite) NSInteger mode;
@property (assign, nonatomic, readwrite) BOOL firstTime;

@property (copy, readwrite, nonatomic) NSMutableData* data;
@property (assign, readwrite, nonatomic) NSNumber* bytesRead;
@property (copy, readwrite, nonatomic) UIImage *screenImg;

@property (copy, readwrite, nonatomic) NSArray *blocks;
@property (copy, readwrite, nonatomic) NSArray *changes;

- (id)initWithName:(NSString *)IP andPort:(NSString *)port;
-(IBAction)connect;
-(IBAction)terminate;
-(IBAction)moveMouseX:(NSInteger)x Y:(NSInteger)y;
-(void) updateScreen;
@end
