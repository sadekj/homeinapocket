//
//  main.m
//  Home In a Pocket
//
//  Created by Sadek Jabr on 4/12/14.
//  Copyright (c) 2014 Sadek Jabr. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
