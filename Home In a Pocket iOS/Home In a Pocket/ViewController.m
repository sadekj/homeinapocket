//
//  ViewController.m
//  Home In a Pocket
//
//  Created by Sadek Jabr on 4/12/14.
//  Copyright (c) 2014 Sadek Jabr. All rights reserved.
//

#import "ViewController.h"
#import "GFXViewController.h"

@interface ViewController ()

@end

@implementation ViewController
@synthesize outputStream = _outputStream;
@synthesize firstTime=_firstTime;
int mode = 6;
- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(IBAction)btnPassword:(id)sender
{
    mode = 7;
    [modeValue setText:@"Password"];
}
-(IBAction)btnPolite:(id)sender
{
    mode = 6;
    [modeValue setText:@"Polite"];
}
-(void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if([segue.identifier isEqualToString:@"GFXViewController"]){
        GFXViewController *destinationViewController = segue.destinationViewController;
        destinationViewController.IP = [txvIP.text copy];
        destinationViewController.port = [txvPort.text copy];
        destinationViewController.strPassword = [txvPassword.text copy];
        destinationViewController.mode = mode;
        destinationViewController.firstTime=!_firstTime;
    }
}
-(void) sendCommand:(NSString *) command
{
    NSString *response  = [NSString stringWithFormat:@"%@\n", command];
	NSData *data = [[NSData alloc] initWithData:[response dataUsingEncoding:NSASCIIStringEncoding]];
	[_outputStream write:[data bytes] maxLength:[data length]];
    
}
-(IBAction)btnSendCustomCommand:(id)sender
{
    [self sendCommand:[txvCutomCommand text]];
}
@end
