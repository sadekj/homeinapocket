package hip.mobilesideandroid.network;

import hip.mobilesideandroid.activities.HIPActivity;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.Socket;
import java.util.List;

import org.apache.http.NameValuePair;

import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;

/**
 * This class provides all the needed network functionalities
 * 
 * @author Sadek Jabr
 * 
 */

public class ConnectionHandler {
	public static final int KEYBOARD = 1, MOUSE = 2, SYSTEM = 3, TERMINATE = 4,
			SCREEN = 5, POLITE = 6, PASSWORD = 7;
	int action;
	static boolean connected;
	DataOutputStream outToServer;
	BufferedReader inFromServer;
	Socket clientSocket;
	InputStream inputStream;
	static ConnectionHandler connectionHandler;
	static boolean[] changes;
	static boolean changed = false;
	public static int blockWidth, blockHeight;
	static int recievedNum = 0, sentNum = 0;
	public static boolean loadFirst = true;
	public static int MODE;

	/**
	 * This function will create a connection with the inputed ip and port
	 * 
	 * @param IP
	 *            , the IP address to connect to.
	 * @param port
	 *            , the port to connect through
	 */
	public boolean connect(String IP, String port) {
		loadFirst = true;
		String[] params = { IP, port };
		new BackLoading().execute(params);
		while (!connected)
			;
		return connected;
	}

	/**
	 * This function will terminate an active connection.
	 * 
	 * @return boolean, true if disconnected, false otherwise
	 * @author Sadek Jabr
	 */
	public boolean terminate() {
		if (clientSocket.isConnected()) {
			try {
				sendCommand("terminate=true");
				connected = false;
				clientSocket.close();
			} catch (IOException e) {
				Log.w(this.getClass().getName(), "Failed termination");
			}
		}
		return !connected;
	}

	/**
	 * This is just to satisfy the single object creation, and will create one
	 * if there is no object from it
	 * 
	 * @return ConnectionHandler, which is the object that is the connection
	 *         created on
	 */
	public static ConnectionHandler getConnectionHandler() {
		if (connectionHandler == null)
			connectionHandler = new ConnectionHandler();
		return connectionHandler;
	}

	/**
	 * This function takes a none parsed command and parses it and send it
	 * 
	 * @param command
	 *            , the command number to be executed
	 * @param params
	 *            , the list of parameters and it's values
	 * @return boolean, true if success sent, false otherwise
	 * @author Sadek Jabr
	 */
	public boolean sendCommand(int command, List<? extends NameValuePair> params) {
		if (!connected || outToServer == null)
			return false;
		String strCommand = "";
		for (NameValuePair pair : params) {
			strCommand += pair.getName() + "=" + pair.getValue();
		}
		return sendCommand(strCommand);
	}

	/**
	 * This function will send a giving message as a new line
	 * 
	 * @param strCommand
	 *            , the message to send
	 * @return boolean, true if success sent, false otherwise
	 * @author Sadek Jabr
	 */
	public boolean sendCommand(String strCommand) {
		if (connected && outToServer != null) {
			try {
				sentNum++;
				Log.i(this.getClass().getName(), sentNum + " "
						+ "Sending command:" + strCommand);
				outToServer.writeBytes(strCommand + "\n");
				return true;
			} catch (IOException e) {
				Log.i(this.getClass().getName(), "Faild sending command "
						+ strCommand);
			}
		}
		return false;
	}

	/**
	 * This function returns one line message from the stream or null if
	 * something is wrong, and will wait for a message if no message is there
	 * 
	 * @return String, the message from the stream
	 * @author Sadek Jabr
	 */
	public String receiveCommand() {
		try {
			if (connected && inFromServer != null && inFromServer.ready()) {
				String command = inFromServer.readLine();
				recievedNum++;
				Log.i("ConnectionHandler", recievedNum + "Command Received:"
						+ command);
				if (command.equals("terminate=true"))
					terminate();
				return command;
			}
		} catch (IOException e) {
			Log.w(this.getClass().getName(), "Faild Receiving data");
		}
		return null;
	}

	public byte[] getScreen() {
		try {
			if (connected && inFromServer != null && inFromServer.ready()) {
				byte[] buffer = new byte[Integer.parseInt(inFromServer
						.readLine())];
				// String str = inFromServer.readLine();
				clientSocket.getInputStream().read(buffer);
				// buffer = str.getBytes();
				Log.i("ConnectionHandler", "buffer= " + buffer);
				return buffer;
			}
		} catch (IOException e) {
			Log.w(this.getClass().getName(), "Faild Receiving data");
		}
		return null;
	}

	/**
	 * Receive an image file from a connected socket and save it to a file.
	 * <p>
	 * the first 4 bytes it receives indicates the file's size
	 * </p>
	 * 
	 * @param is
	 *            InputStream from the connected socket
	 * @param fileName
	 *            Name of the file to save in external storage, without
	 *            File.separator
	 * @return Bitmap representing the image received o null in case of an error
	 * @throws Exception
	 * @see {@link sendFile} for an example how to send the file at other side.
	 * 
	 */
	public Bitmap receiveFile(String fileName) throws Exception {

		String baseDir = Environment.getExternalStorageDirectory()
				.getAbsolutePath();
		String fileInES = baseDir + File.separator + fileName;

		// read 4 bytes containing the file size
		byte[] bSize = new byte[4];
		int offset = 0;
		while (offset < bSize.length) {
			int bRead = inputStream.read(bSize, offset, bSize.length - offset);
			offset += bRead;
		}
		// Convert the 4 bytes to an int
		int fileSize;
		fileSize = (int) (bSize[0] & 0xff) << 24
				| (int) (bSize[1] & 0xff) << 16 | (int) (bSize[2] & 0xff) << 8
				| (int) (bSize[3] & 0xff);

		// buffer to read from the socket
		// 8k buffer is good enough
		byte[] data = new byte[8 * 1024];

		int bToRead;
		FileOutputStream fos = new FileOutputStream(fileInES);
		BufferedOutputStream bos = new BufferedOutputStream(fos);
		while (fileSize > 0) {
			// make sure not to read more bytes than filesize
			if (fileSize > data.length)
				bToRead = data.length;
			else
				bToRead = fileSize;
			int bytesRead = inputStream.read(data, 0, bToRead);
			if (bytesRead > 0) {
				bos.write(data, 0, bytesRead);
				fileSize -= bytesRead;
			}
		}
		bos.close();

		// Convert the received image to a Bitmap
		// If you do not want to return a bitmap comment/delete the folowing
		// lines
		// and make the function to return void or whatever you prefer.
		Bitmap bmp = null;
		FileInputStream fis = new FileInputStream(fileInES);
		try {
			bmp = BitmapFactory.decodeStream(fis);
			return bmp;
		} finally {
			fis.close();
		}
	}

	@SuppressLint("DefaultLocale")
	public Bitmap[] receiveScreen(boolean[] changes, Bitmap[] blocks) {
		try {
			Log.i("connection handler",
					"Expecting segment size and changed segments");
			String command = receiveCommand();
			if (!connected)
				return blocks;
			while (command == null || command.contains(":"))
				command = receiveCommand();
			sendCommand("READY1");
			if (command == null || command.equals("null"))
				return blocks;
			String[] tokens = command.split(",");
			HIPActivity.setRows(Integer.parseInt(tokens[0]));
			HIPActivity.setColumns(Integer.parseInt(tokens[1]));
			blockWidth = Integer.parseInt(tokens[2]);
			blockHeight = Integer.parseInt(tokens[3]);
			blocks = new Bitmap[HIPActivity.getColumns()*HIPActivity.getRows()];
			int blockIndex;
			for (int i = 4; i < tokens.length; i++) {
				blockIndex = Integer.parseInt(tokens[i]);
				changes[blockIndex] = true;
				blocks[blockIndex++] = receiveFile("screen" + blockIndex
						+ "."+HIPActivity.getScreenFormat().toLowerCase());
			}
		} catch (Exception e) {
			Log.w("Connection Handler", "Failed reciving screenblocks");
			e.printStackTrace();
		}
		return blocks;
	}

	/**
	 * This function will return if connected or not
	 * 
	 * @return boolean if there is connection or not.
	 */
	public boolean isConnected() {
		return connected;
	}

	/**
	 * This Internal class handles the connection as a separate thread than the
	 * main thread
	 * 
	 * @author Sadek Jabr
	 * 
	 */
	public class BackLoading extends AsyncTask<String, Void, Socket> {

		protected Socket doInBackground(String... params) {
			try {
				Log.i("backLoading", "Creating socket connection to"
						+ params[0] + ":" + params[1]);
				clientSocket = new Socket(params[0],
						Integer.parseInt(params[1]));
				outToServer = new DataOutputStream(
						clientSocket.getOutputStream());
				inFromServer = new BufferedReader(new InputStreamReader(
						clientSocket.getInputStream()));
				inputStream = clientSocket.getInputStream();
				connected = true;
				return clientSocket;
			} catch (Exception e) {
				e.printStackTrace();
				return null;
			}
		}
	}
}
