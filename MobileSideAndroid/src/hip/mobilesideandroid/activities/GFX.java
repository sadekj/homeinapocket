package hip.mobilesideandroid.activities;

import hip.mobilesideandroid.R;
import hip.mobilesideandroid.network.ConnectionHandler;
import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.v4.view.GestureDetectorCompat;
import android.support.v4.view.MotionEventCompat;
import android.util.FloatMath;
import android.util.Log;
import android.view.Display;
import android.view.GestureDetector.OnDoubleTapListener;
import android.view.GestureDetector.OnGestureListener;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

public class GFX extends HIPActivity implements OnGestureListener,
		OnDoubleTapListener, OnTouchListener {
	SurfaceHolder holder;
	MyBringBack ourView;
	float deviceX, deviceY, respectiveX, respectiveY, oldX, oldY;
	ConnectionHandler ch;
	private GestureDetectorCompat mDetector;

	private static final String DEBUG_TAG = "Gestures";

	public static final String EXTRA_PASSWORD = "hip.mobilesideandroid.activities.GFX.EXTRA_PASSWORD";

	int oldAction = -1;
	Bitmap[] blocks;
	Bitmap mousePointer;
	boolean[] changes = new boolean[16];
	float oldDistance = 0f;
	int screenWidth, screenHeight;
	boolean zooming = false;
	Button btnSettigns;
	Display display;
	Point screen;
	float marginLeft = 0;
	float marginTop = 0;

	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		FrameLayout frameLayout = new FrameLayout(this);
		ourView = new MyBringBack(this);
		ourView.setOnTouchListener(this);
		LinearLayout linearLayout = new LinearLayout(this);

		btnSettigns = new Button(this);

		btnSettigns.setWidth(150);
		btnSettigns.setText("Settings");
		btnSettigns.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
				if (inputMethodManager != null) {
					inputMethodManager.toggleSoftInput(
							InputMethodManager.SHOW_FORCED, 0);
				}
			}
		});

		linearLayout.addView(btnSettigns);

		frameLayout.addView(ourView);
		frameLayout.addView(linearLayout);

		setContentView(frameLayout);

		display = getWindowManager().getDefaultDisplay();
		screen = new Point();
		display.getSize(screen);
		screenWidth = screen.x;
		screenHeight = screen.y - 100;
		ch = ConnectionHandler.getConnectionHandler();
		respectiveX = 0;
		respectiveY = 0;
		deviceX = screenWidth / 2;
		deviceY = screenHeight / 2;
		oldX = deviceX;
		oldY = deviceY;
		btnSettigns.setX(50);
		btnSettigns.setY(50);
		mousePointer = getResizedBitmap(BitmapFactory.decodeResource(
				getResources(), R.drawable.mouse_pointer), 50, 46);
		mDetector = new GestureDetectorCompat(this, this);
		mDetector.setOnDoubleTapListener(this);
	}

	@Override
	protected void onStart() {
		super.onStart();
		if (ConnectionHandler.MODE == ConnectionHandler.POLITE)
			ch.sendCommand("POLITE");
		else if (ConnectionHandler.MODE == ConnectionHandler.PASSWORD) {
			ch.sendCommand("password="
					+ getIntent().getStringExtra(EXTRA_PASSWORD));
		}
		ch.sendCommand("screen=true");
	}

	@Override
	protected void onPause() {
		super.onPause();
		ourView.pause();
	}

	@Override
	protected void onStop() {
		super.onStop();
		ourView.stop();
	}

	@Override
	protected void onResume() {
		super.onResume();
		ourView.resume();
	}

	@SuppressLint("FloatMath")
	@Override
	public boolean onTouch(View v, MotionEvent event) {
		this.mDetector.onTouchEvent(event);
		int action = MotionEventCompat.getActionMasked(event);
		if (event.getPointerCount() == 2) {
			float x = event.getX(0) - event.getX(1);
			float y = event.getY(0) - event.getY(1);

			Log.i("X&Y", x + "&" + y);
			float newDistance = FloatMath.sqrt(x * x + y * y);
			if (newDistance > 500) {
				screenHeight += (newDistance - oldDistance) % 10;
				screenWidth += (newDistance - oldDistance) % 10;
				zooming = true;
				oldDistance = newDistance;
			}
		} else if (event.getPointerCount() == 1) {
			prepareCalculation(event);
			zooming = false;
			switch (action) {
			case (MotionEvent.ACTION_DOWN):
				Log.d(DEBUG_TAG, "Action was DOWN");
				oldAction = action;
				return true;
			case (MotionEvent.ACTION_MOVE):
				Log.d(DEBUG_TAG, "Action was MOVE");

				ConnectionHandler.getConnectionHandler().sendCommand(
						"1:" + (int) this.respectiveX + ","
								+ (int) this.respectiveY);

				oldAction = action;
				return true;
			case (MotionEvent.ACTION_UP):
				Log.d(DEBUG_TAG, "Action was UP");
				if (oldAction == MotionEvent.ACTION_DOWN)
					ConnectionHandler.getConnectionHandler().sendCommand(
							"2:" + (int) this.respectiveX + ","
									+ (int) this.respectiveY);
				oldAction = action;
				return true;
			case (MotionEvent.ACTION_CANCEL):
				Log.d(DEBUG_TAG, "Action was CANCEL");
				oldAction = action;
				return true;
			case (MotionEvent.ACTION_OUTSIDE):
				Log.d(DEBUG_TAG, "Movement occurred outside bounds "
						+ "of current screen element");
				oldAction = action;
				return true;
			default:
				oldAction = action;
				return super.onTouchEvent(event);
			}
		}
		return super.onTouchEvent(event);
	}

	public void loadFirst() {
		try {
			Log.i("GFX", "load first screen.png");
			Canvas canvas = null;
			Bitmap btm = ConnectionHandler.getConnectionHandler().receiveFile(
					"screen.gif");
			btm=getResizedBitmap(btm, screenHeight, screenWidth);
			canvas = holder.lockCanvas();
			if (btm != null)
				canvas.drawBitmap(btm, 0, 0, new Paint());
			holder.unlockCanvasAndPost(canvas);
		} catch (Exception e) {
			e.printStackTrace();
		}
		ConnectionHandler.loadFirst = false;
	}

	public void loadScreen() {
		try {
			float marginLeft = 0;
			float marginTop = 0;
			if (screenWidth > screen.x || screenHeight > screen.y - 100) {
				if (deviceX > screenWidth / 2) {
					marginLeft = (screenWidth / 2) - deviceX;
				}
				if (deviceY > screenHeight / 2) {
					marginTop = (screenHeight / 2) - deviceY;
				}
				if (screenWidth + marginLeft > screen.x)
					this.marginLeft = marginLeft;
				else
					this.marginLeft = screen.x - screenWidth;
				if (screenHeight + marginTop > screen.y - 100)
					this.marginTop = marginTop;
				else
					this.marginTop = screen.y - 100 - screenHeight;
			} else {
				this.marginLeft = 0;
				this.marginTop = 0;
			}
			Canvas canvas = null;
			canvas = holder.lockCanvas(new Rect(screenWidth, 0, screen.x,
					screen.y));
			canvas.drawARGB(255, 0, 0, 0);
			holder.unlockCanvasAndPost(canvas);
			canvas = holder.lockCanvas(new Rect(0, screenHeight, screen.x,
					screen.y));
			canvas.drawARGB(255, 0, 0, 0);
			holder.unlockCanvasAndPost(canvas);
			blocks = ConnectionHandler.getConnectionHandler().receiveScreen(
					changes, blocks);
			if (!zooming && blocks != null)
				for (int i = 0; i < blocks.length; i++) {
					if ((!zooming || changes[i]) && blocks[i] != null) {

						int left = (i % getColumns())
								* (screenWidth / getColumns());
						left += this.marginLeft;
						int top = (i / getRows()) * (screenHeight / getRows());
						top += this.marginTop;
						int right = left + (screenWidth / getColumns());
						int bottom = top + (screenHeight / getRows());

						blocks[i] = getResizedBitmap(blocks[i], screenHeight
								/ getRows(), screenWidth / getColumns());
						canvas = holder.lockCanvas(new Rect(left, top, right,
								bottom));
						canvas.drawBitmap(blocks[i], left, top, new Paint());
						canvas.drawBitmap(mousePointer, deviceX, deviceY, null);
						holder.unlockCanvasAndPost(canvas);
					}
				}
			changes = new boolean[changes.length];
		} catch (Exception e) {
			e.printStackTrace();
		}
		ConnectionHandler.loadFirst = false;
	}

	public Bitmap getResizedBitmap(Bitmap bm, int newHeight, int newWidth) {

		int width = bm.getWidth();

		int height = bm.getHeight();

		float scaleWidth = ((float) newWidth) / width;

		float scaleHeight = ((float) newHeight) / height;

		// CREATE A MATRIX FOR THE MANIPULATION

		Matrix matrix = new Matrix();

		// RESIZE THE BIT MAP

		matrix.postScale(scaleWidth, scaleHeight);

		// RECREATE THE NEW BITMAP

		Bitmap resizedBitmap = Bitmap.createBitmap(bm, 0, 0, width, height,
				matrix, false);

		return resizedBitmap;

	}

	class MyBringBack extends SurfaceView implements Runnable {
		Bitmap gBall;
		Thread thread;
		boolean running = false;

		public MyBringBack(Context context) {
			super(context);
			holder = getHolder();
		}

		public void pause() {
			running = false;
			while (true) {
				try {
					thread.join();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				break;
			}
			thread = null;
		}

		public void stop() {
			running = false;
		}

		public void resume() {
			running = true;
			thread = new Thread(this);
			thread.start();
		}

		@Override
		public void run() {
			while (running
					&& ConnectionHandler.getConnectionHandler().isConnected()) {
				try {
					if (!holder.getSurface().isValid())
						continue;
					if (ConnectionHandler.loadFirst)
						loadFirst();
					loadScreen();
				} catch (Exception e) {
					Log.e("Screen updating", "CARSHED");
				}
			}
		}
	}

	@Override
	public boolean onDown(MotionEvent event) {
		Log.d(DEBUG_TAG, "onDown: " + event.toString());
		return true;
	}

	@Override
	public boolean onFling(MotionEvent event1, MotionEvent event2,
			float velocityX, float velocityY) {
		Log.d(DEBUG_TAG, "onFling: " + event1.toString() + event2.toString());
		return true;
	}

	@Override
	public void onLongPress(MotionEvent event) {
		prepareCalculation(event);
		ConnectionHandler.getConnectionHandler().sendCommand(
				"4:" + (int) this.respectiveX + "," + (int) this.respectiveY);
		Log.d(DEBUG_TAG, "onLongPress: " + event.toString());
	}

	@Override
	public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX,
			float distanceY) {
		if (e1.getPointerCount() == 2 || e2.getPointerCount() == 2 && !zooming)
			ConnectionHandler.getConnectionHandler().sendCommand(
					"5:" + (int) distanceY);
		Log.d(DEBUG_TAG, "onScroll: " + e1.toString() + e2.toString());
		return true;
	}

	@Override
	public void onShowPress(MotionEvent event) {
		Log.d(DEBUG_TAG, "onShowPress: " + event.toString());
	}

	@Override
	public boolean onSingleTapUp(MotionEvent event) {
		Log.d(DEBUG_TAG, "onSingleTapUp: " + event.toString());
		return true;
	}

	@Override
	public boolean onDoubleTap(MotionEvent event) {
		Log.d(DEBUG_TAG, "onDoubleTap: " + event.toString());
		return true;
	}

	@Override
	public boolean onDoubleTapEvent(MotionEvent event) {
		Log.d(DEBUG_TAG, "onDoubleTapEvent: " + event.toString());
		return true;
	}

	@Override
	public boolean onSingleTapConfirmed(MotionEvent event) {
		Log.d(DEBUG_TAG, "onSingleTapConfirmed: " + event.toString());
		ConnectionHandler.getConnectionHandler().sendCommand(
				"2:" + (int) this.respectiveX + "," + (int) this.respectiveY);
		return true;
	}

	private void prepareCalculation(MotionEvent event) {
		this.respectiveX += event.getX() - oldX;
		this.respectiveY += event.getY() - oldY;
		this.deviceX = event.getX();
		this.deviceY = event.getY();
		this.respectiveX -= marginLeft;
		this.respectiveY -= marginTop;
		this.respectiveX = ((this.respectiveX) / screenWidth)
				* (float) ConnectionHandler.blockWidth * 4;
		this.respectiveY = ((this.respectiveY) / screenHeight)
				* (float) ConnectionHandler.blockHeight * 4;
		oldX = this.respectiveX;
		oldY = this.respectiveY;
	}

	@Override
	public boolean dispatchKeyEvent(KeyEvent event) {
		int keyCode = 0;
		if (event.getAction() == KeyEvent.ACTION_UP) {
			keyCode = event.getKeyCode();
			if (keyCode >= 29 && keyCode <= 54)
				keyCode += 36;
			else if (keyCode >= 7 && keyCode <= 16)
				keyCode += 41;
			else if (keyCode == 62)
				keyCode += -30;
			else if (keyCode == 59)
				keyCode += -43;
			else if (keyCode == 66)
				keyCode += -56;
			else if (keyCode == 67)
				keyCode += -59;
			else
				keyCode = 0;
		}
		if (keyCode != 0)
			if (event.isShiftPressed())
				ConnectionHandler.getConnectionHandler().sendCommand("6:p=16");
		ConnectionHandler.getConnectionHandler().sendCommand("6:c=" + keyCode);
		if (event.isShiftPressed())
			ConnectionHandler.getConnectionHandler().sendCommand("6:r=16");
		return super.dispatchKeyEvent(event);
	}
}
