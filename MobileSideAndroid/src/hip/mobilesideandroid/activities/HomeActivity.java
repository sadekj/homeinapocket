package hip.mobilesideandroid.activities;

import hip.mobilesideandroid.R;
import hip.mobilesideandroid.network.ConnectionHandler;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;

public class HomeActivity extends HIPActivity {
	EditText etIP, etPort, etPassword, etCustomCommand;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_home);
		etIP = (EditText) findViewById(R.id.etIP);
		etPort = (EditText) findViewById(R.id.etPort);
		etPassword = (EditText) findViewById(R.id.etpassword);
		etCustomCommand = (EditText) findViewById(R.id.etCustomCommand);
		onRadioButtonClicked(findViewById(R.id.radioPassword));
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.home, menu);
		return true;
	}

	public void connect(View view) {
		ConnectionHandler.getConnectionHandler().connect(
				etIP.getText().toString(), etPort.getText().toString());
		Intent intent = new Intent(this, GFX.class);
		intent.putExtra(GFX.EXTRA_PASSWORD, etPassword.getText().toString());
		startActivity(intent);
	}

	public void terminate(View view) {
		ConnectionHandler.getConnectionHandler().terminate();
	}

	public void sendCustomCommand(View view) {
		ConnectionHandler.getConnectionHandler().sendCommand(
				etCustomCommand.getText().toString());
	}

	public void onRadioButtonClicked(View view) {
		// Is the button now checked?
		boolean checked = ((RadioButton) view).isChecked();

		// Check which radio button was clicked
		switch (view.getId()) {
		case R.id.radioPassword:
			if (checked) {
				ConnectionHandler.MODE = ConnectionHandler.PASSWORD;
				etPassword.setEnabled(true);
				etPassword.setVisibility(View.VISIBLE);
			}
			break;
		case R.id.radioPolite:
			if (checked) {
				ConnectionHandler.MODE = ConnectionHandler.POLITE;
				etPassword.setVisibility(View.INVISIBLE);
				etPassword.setEnabled(false);
			}
			break;
		}
		Log.i("MODE", "" + ConnectionHandler.MODE);
	}
}
