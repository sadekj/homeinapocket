package hip.mobilesideandroid.activities;

import java.util.HashMap;

import android.app.Activity;

/**
 * This class is created to share variables among all activities that extends
 * from it, and to be the root class to all activities
 * 
 * @author Sadek Jabr
 * 
 */
public class HIPActivity extends Activity {
	public static HashMap<String, String> CONFIGURATION = new HashMap<String, String>();

	public void pasue(long millisec) {
		long time = System.currentTimeMillis();
		while (System.currentTimeMillis() - time < millisec)
			;
	}

	/**
	 * 
	 * @return
	 */
	public static int getRows() {
		int value = Integer.parseInt(CONFIGURATION.get("rows"));
		if (value == 0)
			value = 4;
		return value;
	}

	/**
	 * 
	 * @return
	 */
	public static int getColumns() {
		int value = Integer.parseInt(CONFIGURATION.get("columns"));
		if (value == 0)
			value = 4;
		return value;
	}

	/**
	 * 
	 * @return
	 */
	public static String getScreenFormat() {
		String output = CONFIGURATION.get("screenFormat");
		if (output == null)
			output = "GIF";
		return output;
	}

	/**
	 * 
	 * @return
	 */
	public static int getPort() {
		try {
			return Integer.parseInt(CONFIGURATION.get("port"));
		} catch (Exception e) {
			return 6789;
		}
	}

	/**
	 * 
	 * @return
	 */
	public static String getIP() {
		return CONFIGURATION.get("IP");
	}

	/**
	 * 
	 * @return
	 */
	public static String getExternalIP() {
		return CONFIGURATION.get("ExternalIP");
	}

	/**
	 * 
	 * @return
	 */
	public static void setRows(int rows) {
		CONFIGURATION.put("rows", rows + "");
	}

	/**
	 * 
	 * @return
	 */
	public static void setColumns(int columns) {
		CONFIGURATION.put("columns", columns + "");
	}

	/**
	 * 
	 * @return
	 */
	public static void setScreenFormat(String screenFormat) {
		CONFIGURATION.put("screenFormat", screenFormat);
	}

	/**
	 * 
	 * @return
	 */
	public static void setPort(int port) {
		CONFIGURATION.put("port", port + "");
	}
}
